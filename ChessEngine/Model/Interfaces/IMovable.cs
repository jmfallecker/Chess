﻿using ChessEngine.Model.BoardModel;
using ChessEngine.Model.ChessUtility;

namespace ChessEngine.Model.Interfaces
{
    public interface IMovable
    {
        void GenerateMoves(BoardState boardState);
        bool CanMoveTo(ChessPosition position);
        void MoveTo(ChessPosition position);
    }
}
