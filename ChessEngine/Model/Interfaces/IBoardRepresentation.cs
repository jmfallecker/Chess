﻿namespace ChessEngine.Model.Interfaces
{
    public interface IBoardRepresentation
    {
        ulong GetBoardRepresentation();
    }
}
