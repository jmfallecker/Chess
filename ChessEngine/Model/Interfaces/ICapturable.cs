﻿using ChessEngine.Model.BoardModel;
using ChessEngine.Model.ChessUtility;

namespace ChessEngine.Model.Interfaces
{
    public interface ICapturable
    {
        void GenerateCaptures(BoardState boardState, BoardState owningPlayerPieceBitBoard);
        bool CanCaptureAt(ChessPosition location);
        void CaptureAt(ChessPosition location);
    }
}
