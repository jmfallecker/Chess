﻿namespace ChessEngine.Model.Interfaces
{
    interface IThreatening
    {
        bool IsThreateningAt(ulong location, ulong boardState);
    }
}
