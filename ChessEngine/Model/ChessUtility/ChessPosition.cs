﻿using System;

namespace ChessEngine.Model.ChessUtility
{
    [Flags]
    public enum ChessPosition : ulong
    {
        None = 0x0,
        A1 = 0x0000000000000001,
        A2 = 0x0000000000000100,
        A3 = 0x0000000000010000,
        A4 = 0x0000000001000000,
        A5 = 0x0000000100000000,
        A6 = 0x0000010000000000,
        A7 = 0x0001000000000000,
        A8 = 0x0100000000000000,
        B1 = 0x0000000000000002,
        B2 = 0x0000000000000200,
        B3 = 0x0000000000020000,
        B4 = 0x0000000002000000,
        B5 = 0x0000000200000000,
        B6 = 0x0000020000000000,
        B7 = 0x0002000000000000,
        B8 = 0x0200000000000000,
        C1 = 0x0000000000000004,
        C2 = 0x0000000000000400,
        C3 = 0x0000000000040000,
        C4 = 0x0000000004000000,
        C5 = 0x0000000400000000,
        C6 = 0x0000040000000000,
        C7 = 0x0004000000000000,
        C8 = 0x0400000000000000,
        D1 = 0x0000000000000008,
        D2 = 0x0000000000000800,
        D3 = 0x0000000000080000,
        D4 = 0x0000000008000000,
        D5 = 0x0000000800000000,
        D6 = 0x0000080000000000,
        D7 = 0x0008000000000000,
        D8 = 0x0800000000000000,
        E1 = 0x0000000000000010,
        E2 = 0x0000000000001000,
        E3 = 0x0000000000100000,
        E4 = 0x0000000010000000,
        E5 = 0x0000001000000000,
        E6 = 0x0000100000000000,
        E7 = 0x0010000000000000,
        E8 = 0x1000000000000000,
        F1 = 0x0000000000000020,
        F2 = 0x0000000000002000,
        F3 = 0x0000000000200000,
        F4 = 0x0000000020000000,
        F5 = 0x0000002000000000,
        F6 = 0x0000200000000000,
        F7 = 0x0020000000000000,
        F8 = 0x2000000000000000,
        G1 = 0x0000000000000040,
        G2 = 0x0000000000004000,
        G3 = 0x0000000000400000,
        G4 = 0x0000000040000000,
        G5 = 0x0000004000000000,
        G6 = 0x0000400000000000,
        G7 = 0x0040000000000000,
        G8 = 0x4000000000000000,
        H1 = 0x0000000000000080,
        H2 = 0x0000000000008000,
        H3 = 0x0000000000800000,
        H4 = 0x0000000080000000,
        H5 = 0x0000008000000000,
        H6 = 0x0000800000000000,
        H7 = 0x0080000000000000,
        H8 = 0x8000000000000000,

        //FileA = 0x0101010101010101,
        //FileH = 0x8080808080808080,
        Rank1 = 0x00000000000000FF,
        Rank8 = 0xFF00000000000000,
        //A1H8Diagonal = 0x8040201008040201,
        //H1A8Diagonal = 0x0102040810204080,
        Rank4 = 0x00000000FF000000,
        Rank5 = 0x000000FF00000000,
        WhiteStart = 0x000000000000FFFF,
        BlackStart = 0xFFFF000000000000,
    }

    public class ChessPositionUtility
    {
        private enum ChessPositionOrdering : byte
        {
            None = 0,
            A1 = 1,
            A2 = 2,
            A3 = 3,
            A4 = 4,
            A5 = 5,
            A6 = 6,
            A7 = 7,
            A8 = 8,
            B1 = 9,
            B2 = 10,
            B3 = 11,
            B4 = 12,
            B5 = 13,
            B6 = 14,
            B7 = 15,
            B8 = 16,
            C1 = 17,
            C2 = 18,
            C3 = 19,
            C4 = 20,
            C5 = 21,
            C6 = 22,
            C7 = 23,
            C8 = 24,
            D1 = 25,
            D2 = 26,
            D3 = 27,
            D4 = 28,
            D5 = 29,
            D6 = 30,
            D7 = 31,
            D8 = 32,
            E1 = 33,
            E2 = 34,
            E3 = 35,
            E4 = 36,
            E5 = 37,
            E6 = 38,
            E7 = 39,
            E8 = 40,
            F1 = 41,
            F2 = 42,
            F3 = 43,
            F4 = 44,
            F5 = 45,
            F6 = 46,
            F7 = 47,
            F8 = 48,
            G1 = 49,
            G2 = 50,
            G3 = 51,
            G4 = 52,
            G5 = 53,
            G6 = 54,
            G7 = 55,
            G8 = 56,
            H1 = 57,
            H2 = 58,
            H3 = 59,
            H4 = 60,
            H5 = 61,
            H6 = 62,
            H7 = 63,
            H8 = 64
        }

        // TODO: make this more effecient, it's currently as efficient as one step forward, two steps back
        public static Comparison<ChessPosition> Comparison = new Comparison<ChessPosition>((chessPosition1, chessPosition2) =>
        {
            // get names of ChessPositions
            string firstName = Enum.GetName(typeof(ChessPosition), chessPosition1);
            string secondName = Enum.GetName(typeof(ChessPosition), chessPosition2);

            // get value of ChessPositionOrderings
            Enum.TryParse(firstName, out ChessPositionOrdering firstOrdering);
            Enum.TryParse(secondName, out ChessPositionOrdering secondOrdering);

            // compare ChessPositionOrders
            if (firstOrdering > secondOrdering)
                return 1;
            else if (firstOrdering < secondOrdering)
                return -1;
            else // if equal
                return 0;
        });
    }
}
