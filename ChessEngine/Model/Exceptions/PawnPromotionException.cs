﻿using System;
using System.Runtime.Serialization;

namespace ChessEngine.Model.Exceptions
{
    public class PawnPromotionException : Exception
    {
        public PawnPromotionException()
        {
        }

        public PawnPromotionException(string message) : base(message)
        {
        }

        public PawnPromotionException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PawnPromotionException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
