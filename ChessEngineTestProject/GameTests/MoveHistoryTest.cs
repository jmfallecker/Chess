﻿using ChessEngine.Model.ChessUtility;
using ChessEngine.Model.GameModel;
using ChessEngine.Model.MovementModel;
using ChessEngine.Model.PieceModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChessTestProject.GameTests
{
    [TestClass]
    public class MoveHistoryTest
    {
        [TestMethod]
        public void Should_StorePieceTypeMoved_After_MovingAPiece()
        {
            var moveHistory = new MoveHistory();

            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move
                {
                    StartingPosition = ChessPosition.A1,
                    EndingPosition = ChessPosition.A3
                });

            Assert.AreEqual(typeof(Pawn), moveHistory.Moves[0].pieceType);
        }

        [TestMethod]
        public void Should_ReturnNumberOfMoves()
        {
            var moveHistory = new MoveHistory();
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move
                {
                    StartingPosition = ChessPosition.A1,
                    EndingPosition = ChessPosition.A3
                });

            Assert.AreEqual(1, moveHistory.Count);
        }

        [TestMethod]
        public void Should_ReturnFalse_With_NoCaptureInLastFiftyMoves()
        {
            var moveHistory = new MoveHistory();
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move
                {
                    StartingPosition = ChessPosition.A1,
                    EndingPosition = ChessPosition.A3
                });

            Assert.IsFalse(moveHistory.WasPieceCapturedInLastFiftyMoves());
        }

        [TestMethod]
        public void Should_ReturnTrue_With_CaptureInLastFiftyMoves()
        {
            var moveHistory = new MoveHistory();
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move
                {
                    StartingPosition = ChessPosition.A1,
                    EndingPosition = ChessPosition.A3
                });
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Capture());

            Assert.IsTrue(moveHistory.WasPieceCapturedInLastFiftyMoves());
        }

        [TestMethod]
        public void Should_ReturnFalse_With_CaptureBeforeFiftyMovesAgo()
        {
            var moveHistory = new MoveHistory();
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Capture());

            #region Add 50 Moves (No captures)
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            #endregion

            Assert.IsFalse(moveHistory.WasPieceCapturedInLastFiftyMoves());

        }

        [TestMethod]
        public void Should_ReturnTrue_When_PawnMovedInLastFiftyMoves()
        {
            var moveHistory = new MoveHistory();

            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());

            Assert.IsTrue(moveHistory.WasPawnMovedInLastFiftyMoves());
        }

        [TestMethod]
        public void Should_ReturnFalse_When_NoPawnMovedInLastFiftyMoves()
        {
            var moveHistory = new MoveHistory();

            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());

            Assert.IsFalse(moveHistory.WasPawnMovedInLastFiftyMoves());
        }

        [TestMethod]
        public void Should_ReturnFalse_When_PawnMovedMoreThanFiftyMovesAgo()
        {
            var moveHistory = new MoveHistory();

            moveHistory.Add(
                new Pawn(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            #region Move Queens (No Pawn moves) (wow sure are a lot of queens in this game)
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move()); moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move()); moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move()); moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move()); moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            moveHistory.Add(
                new Queen(ChessPosition.A1, ChessColor.Colors.White),
                new Move());
            #endregion

            Assert.IsFalse(moveHistory.WasPawnMovedInLastFiftyMoves());
        }
    }
}
