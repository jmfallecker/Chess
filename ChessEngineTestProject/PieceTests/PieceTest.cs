﻿using ChessEngine.Model.ChessUtility;
using ChessEngine.Model.PieceModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChessTestProject.PieceTests
{
    [TestClass]
    public class PieceTest
    {
        [TestMethod]
        public void Should_ChangeLocation_When_PieceIsMoved()
        {
            Piece p = new Pawn(ChessPosition.A2, ChessColor.Colors.White);

            Assert.AreNotEqual(p.Location, ChessPosition.A3);

            p.MoveTo(ChessPosition.A3);

            Assert.AreEqual(p.Location, ChessPosition.A3);
        }

        [TestMethod]
        public void Should_UpdateHasMoved_After_PieceMakesFirstMove()
        {
            Piece p = new Pawn(ChessPosition.A2, ChessColor.Colors.White);
            Assert.IsFalse(p.HasMoved);

            p.MoveTo(ChessPosition.A4);

            Assert.IsTrue(p.HasMoved);
        }

        [TestMethod]
        public void Should_UpdateHasMoved_After_PieceCapturesAsFirstMove()
        {
            Piece p = new Pawn(ChessPosition.A2, ChessColor.Colors.White);
            Assert.IsFalse(p.HasMoved);

            p.CaptureAt(ChessPosition.A4);

            Assert.IsTrue(p.HasMoved);
        }

    }
}
