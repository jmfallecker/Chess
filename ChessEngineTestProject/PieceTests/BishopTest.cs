﻿using ChessEngine.Model.BoardModel;
using ChessEngine.Model.ChessUtility;
using ChessEngine.Model.PieceModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChessTestProject.PieceTests
{
    [TestClass]
    public class BishopTest
    {
        #region Movement Tests

        [TestMethod]
        public void Should_ReturnTrue_When_BishopMovingNorthEast()
        {
            var board = new Board(new BoardState(), new BoardState());
            var bishop = new Bishop(ChessPosition.D4, ChessColor.Colors.Black);

            bishop.GenerateMoves(board.State);

            Assert.IsTrue(bishop.CanMoveTo(ChessPosition.F6));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_BishopMovingNorthWest()
        {
            var board = new Board(new BoardState(), new BoardState());
            var bishop = new Bishop(ChessPosition.D4, ChessColor.Colors.Black);

            bishop.GenerateMoves(board.State);

            Assert.IsTrue(bishop.CanMoveTo(ChessPosition.B6));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_BishopMovingSouthEast()
        {
            var board = new Board(new BoardState(), new BoardState());
            var bishop = new Bishop(ChessPosition.D4, ChessColor.Colors.Black);

            bishop.GenerateMoves(board.State);

            Assert.IsTrue(bishop.CanMoveTo(ChessPosition.F2));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_BishopMovingSouthWest()
        {
            var board = new Board(new BoardState(), new BoardState());
            var bishop = new Bishop(ChessPosition.D4, ChessColor.Colors.Black);

            bishop.GenerateMoves(board.State);

            Assert.IsTrue(bishop.CanMoveTo(ChessPosition.B2));
        }

        [TestMethod]
        public void Should_ReturnFalse_WhenBishopJumpingPieceNorthEast()
        {
            var board = new Board(new BoardState(), new BoardState());
            var bishop = new Bishop(ChessPosition.D4, ChessColor.Colors.Black);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.E5);

            bishop.GenerateMoves(board.State);

            Assert.IsFalse(bishop.CanMoveTo(ChessPosition.F6));
        }

        [TestMethod]
        public void Should_ReturnFalse_WhenBishopJumpingPieceNorthWest()
        {
            var board = new Board(new BoardState(), new BoardState());
            var bishop = new Bishop(ChessPosition.D4, ChessColor.Colors.Black);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.C5);

            bishop.GenerateMoves(board.State);

            Assert.IsFalse(bishop.CanMoveTo(ChessPosition.B6));
        }

        [TestMethod]
        public void Should_ReturnFalse_WhenBishopJumpingPieceSouthEast()
        {
            var board = new Board(new BoardState(), new BoardState());
            var bishop = new Bishop(ChessPosition.D4, ChessColor.Colors.Black);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.E3);

            bishop.GenerateMoves(board.State);

            Assert.IsFalse(bishop.CanMoveTo(ChessPosition.F2));
        }

        [TestMethod]
        public void Should_ReturnFalse_WhenBishopJumpingPieceSouthWest()
        {
            var board = new Board(new BoardState(), new BoardState());
            var bishop = new Bishop(ChessPosition.D4, ChessColor.Colors.Black);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.C3);

            bishop.GenerateMoves(board.State);

            Assert.IsFalse(bishop.CanMoveTo(ChessPosition.B2));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_BishopMovingNorth()
        {
            var board = new Board(new BoardState(), new BoardState());
            var bishop = new Bishop(ChessPosition.D4, ChessColor.Colors.Black);

            bishop.GenerateMoves(board.State);

            Assert.IsFalse(bishop.CanMoveTo(ChessPosition.D7));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_BishopMovingSouth()
        {
            var board = new Board(new BoardState(), new BoardState());
            var bishop = new Bishop(ChessPosition.D4, ChessColor.Colors.Black);

            bishop.GenerateMoves(board.State);

            Assert.IsFalse(bishop.CanMoveTo(ChessPosition.D2));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_BishopMovingEast()
        {
            var board = new Board(new BoardState(), new BoardState());
            var bishop = new Bishop(ChessPosition.D4, ChessColor.Colors.Black);

            bishop.GenerateMoves(board.State);

            Assert.IsFalse(bishop.CanMoveTo(ChessPosition.F4));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_BishopMovingWest()
        {
            var board = new Board(new BoardState(), new BoardState());
            var bishop = new Bishop(ChessPosition.D4, ChessColor.Colors.Black);

            bishop.GenerateMoves(board.State);

            Assert.IsFalse(bishop.CanMoveTo(ChessPosition.B4));
        }

        #endregion

        #region Capture Tests

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingNorthEastAtUnoccupiedLocation()
        {
            Piece piece = new Bishop(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(piece.Location), new BoardState());

            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsFalse(piece.CanCaptureAt(ChessPosition.F8));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingNorthWestAtUnoccupiedLocation()
        {
            Piece piece = new Bishop(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(piece.Location), new BoardState());

            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsFalse(piece.CanCaptureAt(ChessPosition.A7));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingSouthEastAtUnoccupiedLocation()
        {
            Piece piece = new Bishop(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(piece.Location), new BoardState());

            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsFalse(piece.CanCaptureAt(ChessPosition.G1));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingSouthWestAtUnoccupiedLocation()
        {
            Piece piece = new Bishop(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(piece.Location), new BoardState());

            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsFalse(piece.CanCaptureAt(ChessPosition.A3));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingNorthEastAtOccupiedLocation()
        {
            Piece piece = new Bishop(ChessPosition.C5, ChessColor.Colors.White);
            var enemyPiecePosition = ChessPosition.F8;

            var board = new Board(new BoardState(piece.Location), new BoardState(enemyPiecePosition));

            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsTrue(piece.CanCaptureAt(enemyPiecePosition));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingNorthWestAtOccupiedLocation()
        {
            Piece piece = new Bishop(ChessPosition.C5, ChessColor.Colors.White);
            var enemyPiecePosition = ChessPosition.A7;
            var board = new Board(new BoardState(piece.Location), new BoardState(enemyPiecePosition));

            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsTrue(piece.CanCaptureAt(enemyPiecePosition));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingSouthEastAtOccupiedLocation()
        {
            Piece piece = new Bishop(ChessPosition.C5, ChessColor.Colors.White);
            var enemyPiecePosition = ChessPosition.G1;
            var board = new Board(new BoardState(piece.Location), new BoardState(enemyPiecePosition));

            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsTrue(piece.CanCaptureAt(enemyPiecePosition));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingSouthWestAtOccupiedLocation()
        {
            Piece piece = new Bishop(ChessPosition.C5, ChessColor.Colors.White);
            var enemyPiecePosition = ChessPosition.A3;
            var board = new Board(new BoardState(piece.Location), new BoardState(enemyPiecePosition));

            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsTrue(piece.CanCaptureAt(enemyPiecePosition));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingNorthEastAtOccupiedLocationWithFriendliesInBetween()
        {
            var enemyPieceLocation = ChessPosition.H8;
            var enemyPieceLocations = new BoardState
            {
                enemyPieceLocation
            };

            Piece bishop = new Bishop(ChessPosition.E5, ChessColor.Colors.Black);
            var friendlyPieceLocation = new BoardState
            {
                bishop.Location,
                ChessPosition.F6,
                ChessPosition.G7
            };

            var board = new Board(enemyPieceLocations, friendlyPieceLocation);
            bishop.GenerateCaptures(board.State, friendlyPieceLocation);

            Assert.IsFalse(bishop.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingNorthWestAtOccupiedLocationWithFriendliesInBetween()
        {
            var enemyPieceLocation = ChessPosition.B8;
            var enemyPieceLocations = new BoardState
            {
                enemyPieceLocation
            };

            Piece bishop = new Bishop(ChessPosition.E5, ChessColor.Colors.Black);
            var friendlyPieceLocation = new BoardState
            {
                bishop.Location,
                ChessPosition.D6,
                ChessPosition.C7
            };

            var board = new Board(enemyPieceLocations, friendlyPieceLocation);
            bishop.GenerateCaptures(board.State, friendlyPieceLocation);

            Assert.IsFalse(bishop.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingSouthEastAtOccupiedLocationWithFriendliesInBetween()
        {
            var enemyPieceLocation = ChessPosition.H1;
            var enemyPieceLocations = new BoardState
            {
                enemyPieceLocation
            };

            Piece bishop = new Bishop(ChessPosition.E4, ChessColor.Colors.Black);
            var friendlyPieceLocation = new BoardState
            {
                bishop.Location,
                ChessPosition.F3,
                ChessPosition.G2
            };

            var board = new Board(enemyPieceLocations, friendlyPieceLocation);
            bishop.GenerateCaptures(board.State, friendlyPieceLocation);

            Assert.IsFalse(bishop.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingSouthWestAtOccupiedLocationWithFriendliesInBetween()
        {
            var enemyPieceLocation = ChessPosition.B1;
            var enemyPieceLocations = new BoardState
            {
                enemyPieceLocation
            };

            Piece bishop = new Bishop(ChessPosition.E4, ChessColor.Colors.Black);
            var friendlyPieceLocation = new BoardState
            {
                bishop.Location,
                ChessPosition.D3,
                ChessPosition.C2
            };

            var board = new Board(enemyPieceLocations, friendlyPieceLocation);
            bishop.GenerateCaptures(board.State, friendlyPieceLocation);

            Assert.IsFalse(bishop.CanCaptureAt(enemyPieceLocation));
        }


        #endregion

        #region Threaten Tests

        [TestMethod]
        public void Should_ReturnTrue_For_ThreatenedSquares()
        {
            Piece piece = new Bishop(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(piece.Location), new BoardState());

            piece.GenerateThreatened(board.State, new BoardState(piece.Location));

            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.D6));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.E7));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.F8));

            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.B4));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.A3));

            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.B6));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.A7));

            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.D4));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.E3));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.F2));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.G1));
        }

        #endregion
    }
}
