﻿using ChessEngine.Model.BoardModel;
using ChessEngine.Model.ChessUtility;
using ChessEngine.Model.PieceModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChessTestProject.PieceTests
{
    [TestClass]
    public class KnightTest
    {
        #region Movement Tests

        [TestMethod]
        public void Should_ReturnTrue_When_KnightMovesNorthNorthEast()
        {
            var board = new Board(new BoardState(), new BoardState());
            var knight = new Knight(ChessPosition.D4, ChessColor.Colors.White);

            knight.GenerateMoves(board.State);
            Assert.IsTrue(knight.CanMoveTo(ChessPosition.E6));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_KnightMovesNorthNorthWest()
        {
            var board = new Board(new BoardState(), new BoardState());
            var knight = new Knight(ChessPosition.D4, ChessColor.Colors.White);

            knight.GenerateMoves(board.State);
            Assert.IsTrue(knight.CanMoveTo(ChessPosition.C6));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_KnightMovesSouthSouthEast()
        {
            var board = new Board(new BoardState(), new BoardState());
            var knight = new Knight(ChessPosition.D4, ChessColor.Colors.White);

            knight.GenerateMoves(board.State);
            Assert.IsTrue(knight.CanMoveTo(ChessPosition.E2));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_KnightMovesSouthSouthWest()
        {
            var board = new Board(new BoardState(), new BoardState());
            var knight = new Knight(ChessPosition.D4, ChessColor.Colors.White);

            knight.GenerateMoves(board.State);
            Assert.IsTrue(knight.CanMoveTo(ChessPosition.C2));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_KnightMovesEastNorthEast()
        {
            var board = new Board(new BoardState(), new BoardState());
            var knight = new Knight(ChessPosition.D4, ChessColor.Colors.White);

            knight.GenerateMoves(board.State);
            Assert.IsTrue(knight.CanMoveTo(ChessPosition.F5));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_KnightMovesEastSouthEast()
        {
            var board = new Board(new BoardState(), new BoardState());
            var knight = new Knight(ChessPosition.D4, ChessColor.Colors.White);

            knight.GenerateMoves(board.State);
            Assert.IsTrue(knight.CanMoveTo(ChessPosition.F3));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_KnightMovesWestNorthWest()
        {
            var board = new Board(new BoardState(), new BoardState());
            var knight = new Knight(ChessPosition.D4, ChessColor.Colors.White);

            knight.GenerateMoves(board.State);
            Assert.IsTrue(knight.CanMoveTo(ChessPosition.B5));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_KnightMovesWestSouthWest()
        {
            var board = new Board(new BoardState(), new BoardState());
            var knight = new Knight(ChessPosition.D4, ChessColor.Colors.White);

            knight.GenerateMoves(board.State);
            Assert.IsTrue(knight.CanMoveTo(ChessPosition.B3));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_KnightMovesToOccupiedSquareNorthNorthEast()
        {
            var board = new Board(new BoardState(), new BoardState());
            board.Add(ChessPosition.E6);

            var knight = new Knight(ChessPosition.D4, ChessColor.Colors.White);
            knight.GenerateMoves(board.State);

            Assert.IsFalse(knight.CanMoveTo(ChessPosition.E6));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_KnightMovesToOccupiedSquareNorthNorthWest()
        {
            var board = new Board(new BoardState(), new BoardState());
            board.Add(ChessPosition.C6);

            var knight = new Knight(ChessPosition.D4, ChessColor.Colors.White);
            knight.GenerateMoves(board.State);

            Assert.IsFalse(knight.CanMoveTo(ChessPosition.C6));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_KnightMovesToOccupiedSquareSouthSouthEast()
        {
            var board = new Board(new BoardState(), new BoardState());
            board.Add(ChessPosition.E2);

            var knight = new Knight(ChessPosition.D4, ChessColor.Colors.White);
            knight.GenerateMoves(board.State);

            Assert.IsFalse(knight.CanMoveTo(ChessPosition.E2));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_KnightMovesToOccupiedSquareSouthSouthWest()
        {
            var board = new Board(new BoardState(), new BoardState());
            board.Add(ChessPosition.C2);

            var knight = new Knight(ChessPosition.D4, ChessColor.Colors.White);
            knight.GenerateMoves(board.State);

            Assert.IsFalse(knight.CanMoveTo(ChessPosition.C2));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_KnightMovesToOccupiedSquareEastNorthEast()
        {
            var board = new Board(new BoardState(), new BoardState());
            board.Add(ChessPosition.F5);

            var knight = new Knight(ChessPosition.D4, ChessColor.Colors.White);
            knight.GenerateMoves(board.State);

            Assert.IsFalse(knight.CanMoveTo(ChessPosition.F5));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_KnightMovesToOccupiedSquareEastSouthEast()
        {
            var board = new Board(new BoardState(), new BoardState());
            board.Add(ChessPosition.F3);

            var knight = new Knight(ChessPosition.D4, ChessColor.Colors.White);
            knight.GenerateMoves(board.State);

            Assert.IsFalse(knight.CanMoveTo(ChessPosition.F3));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_KnightMovesToOccupiedSquareWestNorthWest()
        {
            var board = new Board(new BoardState(), new BoardState());
            board.Add(ChessPosition.B5);

            var knight = new Knight(ChessPosition.D4, ChessColor.Colors.White);
            knight.GenerateMoves(board.State);

            Assert.IsFalse(knight.CanMoveTo(ChessPosition.B5));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_KnightMovesToOccupiedSquareWestSouthWest()
        {
            var board = new Board(new BoardState(), new BoardState());
            board.Add(ChessPosition.B3);

            var knight = new Knight(ChessPosition.D4, ChessColor.Colors.White);
            knight.GenerateMoves(board.State);

            Assert.IsFalse(knight.CanMoveTo(ChessPosition.B3));
        }

        #endregion

        #region Capture Tests

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingNorthNorthEastAtUnoccupiedLocation()
        {
            var board = new Board(new BoardState(ChessPosition.C4), new BoardState());
            var knight = new Knight(ChessPosition.C4, ChessColor.Colors.White);

            knight.GenerateCaptures(board.State, new BoardState(knight.Location));
            Assert.IsFalse(knight.CanCaptureAt(ChessPosition.D6));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingNorthNorthWestAtUnoccupiedLocation()
        {
            var board = new Board(new BoardState(ChessPosition.C4), new BoardState());
            var knight = new Knight(ChessPosition.C4, ChessColor.Colors.White);

            knight.GenerateCaptures(board.State, new BoardState(knight.Location));
            Assert.IsFalse(knight.CanCaptureAt(ChessPosition.B6));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingSouthSouthEastAtUnoccupiedLocation()
        {
            var board = new Board(new BoardState(ChessPosition.C5), new BoardState());
            var knight = new Knight(ChessPosition.C5, ChessColor.Colors.White);

            knight.GenerateCaptures(board.State, new BoardState(knight.Location));
            Assert.IsFalse(knight.CanCaptureAt(ChessPosition.D3));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingSouthSouthWestAtUnoccupiedLocation()
        {
            var board = new Board(new BoardState(ChessPosition.C5), new BoardState());
            var knight = new Knight(ChessPosition.C5, ChessColor.Colors.White);

            knight.GenerateCaptures(board.State, new BoardState(knight.Location));
            Assert.IsFalse(knight.CanCaptureAt(ChessPosition.B3));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingEastNorthEastAtUnoccupiedLocation()
        {
            var board = new Board(new BoardState(ChessPosition.C4), new BoardState());
            var knight = new Knight(ChessPosition.C4, ChessColor.Colors.White);

            knight.GenerateCaptures(board.State, new BoardState(knight.Location));
            Assert.IsFalse(knight.CanCaptureAt(ChessPosition.E5));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingEastSouthEastAtUnoccupiedLocation()
        {
            var board = new Board(new BoardState(ChessPosition.C4), new BoardState());
            var knight = new Knight(ChessPosition.C4, ChessColor.Colors.White);

            knight.GenerateCaptures(board.State, new BoardState(knight.Location));
            Assert.IsFalse(knight.CanCaptureAt(ChessPosition.E3));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingWestNorthWestAtUnoccupiedLocation()
        {
            var board = new Board(new BoardState(ChessPosition.C4), new BoardState());
            var knight = new Knight(ChessPosition.C4, ChessColor.Colors.White);

            knight.GenerateCaptures(board.State, new BoardState(knight.Location));
            Assert.IsFalse(knight.CanCaptureAt(ChessPosition.A5));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingWestSouthWestAtUnoccupiedLocation()
        {
            var board = new Board(new BoardState(ChessPosition.C4), new BoardState());
            var knight = new Knight(ChessPosition.C4, ChessColor.Colors.White);

            knight.GenerateCaptures(board.State, new BoardState(knight.Location));
            Assert.IsFalse(knight.CanCaptureAt(ChessPosition.A3));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingNorthNorthEastAtOccupiedLocation()
        {
            var board = new Board(new BoardState(ChessPosition.C4), new BoardState());
            board.Add(ChessPosition.D6);

            var knight = new Knight(ChessPosition.C4, ChessColor.Colors.White);

            knight.GenerateCaptures(board.State, new BoardState(knight.Location));
            Assert.IsTrue(knight.CanCaptureAt(ChessPosition.D6));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingNorthNorthWestAtOccupiedLocation()
        {
            var board = new Board(new BoardState(ChessPosition.C4), new BoardState());
            board.Add(ChessPosition.B6);

            var knight = new Knight(ChessPosition.C4, ChessColor.Colors.White);

            knight.GenerateCaptures(board.State, new BoardState(knight.Location));
            Assert.IsTrue(knight.CanCaptureAt(ChessPosition.B6));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingSouthSouthEastAtOccupiedLocation()
        {
            var board = new Board(new BoardState(ChessPosition.C5), new BoardState());
            board.Add(ChessPosition.D3);

            var knight = new Knight(ChessPosition.C5, ChessColor.Colors.White);
            knight.GenerateCaptures(board.State, new BoardState(knight.Location));

            Assert.IsTrue(knight.CanCaptureAt(ChessPosition.D3));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingSouthSouthWestAtOccupiedLocation()
        {
            var board = new Board(new BoardState(ChessPosition.C5), new BoardState());
            board.Add(ChessPosition.B3);

            var knight = new Knight(ChessPosition.C5, ChessColor.Colors.White);

            knight.GenerateCaptures(board.State, new BoardState(knight.Location));
            Assert.IsTrue(knight.CanCaptureAt(ChessPosition.B3));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingEastNorthEastAtOccupiedLocation()
        {
            var board = new Board(new BoardState(ChessPosition.C4), new BoardState());
            board.Add(ChessPosition.E5);

            var knight = new Knight(ChessPosition.C4, ChessColor.Colors.White);

            knight.GenerateCaptures(board.State, new BoardState(knight.Location));
            Assert.IsTrue(knight.CanCaptureAt(ChessPosition.E5));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingEastSouthEastAtOccupiedLocation()
        {
            var board = new Board(new BoardState(ChessPosition.C4), new BoardState());
            board.Add(ChessPosition.E3);

            var knight = new Knight(ChessPosition.C4, ChessColor.Colors.White);

            knight.GenerateCaptures(board.State, new BoardState(knight.Location));
            Assert.IsTrue(knight.CanCaptureAt(ChessPosition.E3));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingWestNorthWestAtOccupiedLocation()
        {
            var board = new Board(new BoardState(ChessPosition.C4), new BoardState());
            board.Add(ChessPosition.A5);

            var knight = new Knight(ChessPosition.C4, ChessColor.Colors.White);

            knight.GenerateCaptures(board.State, new BoardState(knight.Location));
            Assert.IsTrue(knight.CanCaptureAt(ChessPosition.A5));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingWestSouthWestAtOccupiedLocation()
        {
            var board = new Board(new BoardState(ChessPosition.C4), new BoardState());
            board.Add(ChessPosition.A3);

            var knight = new Knight(ChessPosition.C4, ChessColor.Colors.White);

            knight.GenerateCaptures(board.State, new BoardState(knight.Location));
            Assert.IsTrue(knight.CanCaptureAt(ChessPosition.A3));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingAtFriendlyOccupiedLocation()
        {
            var knight = new Knight(ChessPosition.C4, ChessColor.Colors.White);

            Assert.IsFalse(knight.CanCaptureAt(ChessPosition.C2));
        }

        #endregion

        #region Threaten Tests

        [TestMethod]
        public void Should_ReturnTrue_For_ThreatenedSquares()
        {
            Piece knight = new Knight(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(knight.Location), new BoardState());

            knight.GenerateThreatened(board.State, new BoardState(knight.Location));

            Assert.IsTrue(knight.IsThreateningAt(ChessPosition.D7));
            Assert.IsTrue(knight.IsThreateningAt(ChessPosition.B7));

            Assert.IsTrue(knight.IsThreateningAt(ChessPosition.E6));
            Assert.IsTrue(knight.IsThreateningAt(ChessPosition.A6));

            Assert.IsTrue(knight.IsThreateningAt(ChessPosition.E4));
            Assert.IsTrue(knight.IsThreateningAt(ChessPosition.A4));

            Assert.IsTrue(knight.IsThreateningAt(ChessPosition.D3));
            Assert.IsTrue(knight.IsThreateningAt(ChessPosition.B3));
        }

        #endregion
    }
}
