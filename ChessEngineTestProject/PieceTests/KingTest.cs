﻿using ChessEngine.Model.BoardModel;
using ChessEngine.Model.ChessUtility;
using ChessEngine.Model.PieceModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChessTestProject.PieceTests
{
    [TestClass]
    public class KingTest
    {
        #region Movement Tests

        [TestMethod]
        public void Should_ReturnTrue_When_KingMovingNorthEast()
        {
            var board = new Board(new BoardState(), new BoardState());
            var king = new King(ChessPosition.D4, ChessColor.Colors.Black);

            king.GenerateMoves(board.State);

            Assert.IsTrue(king.CanMoveTo(ChessPosition.E5));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_KingMovingNorthWest()
        {
            var board = new Board(new BoardState(), new BoardState());
            var king = new King(ChessPosition.D4, ChessColor.Colors.Black);

            king.GenerateMoves(board.State);

            Assert.IsTrue(king.CanMoveTo(ChessPosition.C5));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_KingMovingSouthEast()
        {
            var board = new Board(new BoardState(), new BoardState());
            var king = new King(ChessPosition.D4, ChessColor.Colors.Black);

            king.GenerateMoves(board.State);

            Assert.IsTrue(king.CanMoveTo(ChessPosition.E3));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_KingMovingSouthWest()
        {
            var board = new Board(new BoardState(), new BoardState());
            var king = new King(ChessPosition.D4, ChessColor.Colors.Black);

            king.GenerateMoves(board.State);

            Assert.IsTrue(king.CanMoveTo(ChessPosition.C3));
        }

        [TestMethod]
        public void Should_ReturnFalse_WhenKingMovingToOccupiedSquareNorthEast()
        {
            var board = new Board(new BoardState(), new BoardState());
            var king = new King(ChessPosition.D4, ChessColor.Colors.Black);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.E5);

            king.GenerateMoves(board.State);

            Assert.IsFalse(king.CanMoveTo(ChessPosition.E5));
        }

        [TestMethod]
        public void Should_ReturnFalse_WhenKingMovingToOccupiedSquareNorthWest()
        {
            var board = new Board(new BoardState(), new BoardState());
            var king = new King(ChessPosition.D4, ChessColor.Colors.Black);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.C5);

            king.GenerateMoves(board.State);

            Assert.IsFalse(king.CanMoveTo(ChessPosition.C5));
        }

        [TestMethod]
        public void Should_ReturnFalse_WhenKingMovingToOccupiedSquareSouthEast()
        {
            var board = new Board(new BoardState(), new BoardState());
            var king = new King(ChessPosition.D4, ChessColor.Colors.Black);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.E3);

            king.GenerateMoves(board.State);

            Assert.IsFalse(king.CanMoveTo(ChessPosition.E3));
        }

        [TestMethod]
        public void Should_ReturnFalse_WhenKingMovingToOccupiedSquareSouthWest()
        {
            var board = new Board(new BoardState(), new BoardState());
            var king = new King(ChessPosition.D4, ChessColor.Colors.Black);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.C3);

            king.GenerateMoves(board.State);

            Assert.IsFalse(king.CanMoveTo(ChessPosition.C3));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_KingIsMovingNorth()
        {
            var board = new Board(new BoardState(), new BoardState());

            var king = new King(ChessPosition.A1, ChessColor.Colors.White);
            king.GenerateMoves(board.State);

            Assert.IsTrue(king.CanMoveTo(ChessPosition.A2));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_KingIsMovingSouth()
        {
            var board = new Board(new BoardState(), new BoardState());

            var king = new King(ChessPosition.A8, ChessColor.Colors.White);
            king.GenerateMoves(board.State);

            Assert.IsTrue(king.CanMoveTo(ChessPosition.A7));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_KingIsMovingEast()
        {
            var board = new Board(new BoardState(), new BoardState());

            var king = new King(ChessPosition.A1, ChessColor.Colors.White);
            king.GenerateMoves(board.State);

            Assert.IsTrue(king.CanMoveTo(ChessPosition.B1));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_KingIsMovingWest()
        {
            var board = new Board(new BoardState(), new BoardState());

            var king = new King(ChessPosition.H1, ChessColor.Colors.White);
            king.GenerateMoves(board.State);

            Assert.IsTrue(king.CanMoveTo(ChessPosition.G1));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_KingIsMovingToOccupiedSquareNorth()
        {
            var board = new Board(new BoardState(), new BoardState());

            var king = new King(ChessPosition.A1, ChessColor.Colors.White);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.A2);

            king.GenerateMoves(board.State);

            Assert.IsFalse(king.CanMoveTo(ChessPosition.A2));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_KingIsMovingToOccupiedSquareSouth()
        {
            var board = new Board(new BoardState(), new BoardState());

            var king = new King(ChessPosition.A8, ChessColor.Colors.White);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.A7);

            king.GenerateMoves(board.State);

            Assert.IsFalse(king.CanMoveTo(ChessPosition.A7));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_KingIsMovingToOccupiedSquareEast()
        {
            var board = new Board(new BoardState(), new BoardState());
            var king = new King(ChessPosition.A1, ChessColor.Colors.White);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.B1);

            king.GenerateMoves(board.State);

            Assert.IsFalse(king.CanMoveTo(ChessPosition.B1));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_KingIsMovingToOccupiedSquareWest()
        {
            var board = new Board(new BoardState(), new BoardState());

            var king = new King(ChessPosition.H1, ChessColor.Colors.White);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.G1);

            king.GenerateMoves(board.State);

            Assert.IsFalse(king.CanMoveTo(ChessPosition.G1));
        }

        #endregion

        #region Capture Tests

        [TestMethod]
        public void Should_ReturnTrue_When_KingCapturesNorthAtOccupiedLocation()
        {
            var king = new King(ChessPosition.C4, ChessColor.Colors.Black);
            var board = new Board(new BoardState(ChessPosition.C5), new BoardState(ChessPosition.C4));

            king.GenerateCaptures(board.State, new BoardState(king.Location));

            Assert.IsTrue(king.CanCaptureAt(ChessPosition.C5));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_KingCapturesSouthAtOccupiedLocation()
        {
            var king = new King(ChessPosition.C4, ChessColor.Colors.Black);
            var board = new Board(new BoardState(ChessPosition.C3), new BoardState(ChessPosition.C4));

            king.GenerateCaptures(board.State, new BoardState(king.Location));

            Assert.IsTrue(king.CanCaptureAt(ChessPosition.C3));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_KingCapturesEastAtOccupiedLocation()
        {
            var king = new King(ChessPosition.C5, ChessColor.Colors.Black);
            var board = new Board(new BoardState(ChessPosition.B5), new BoardState(ChessPosition.C5));

            king.GenerateCaptures(board.State, new BoardState(king.Location));

            Assert.IsTrue(king.CanCaptureAt(ChessPosition.B5));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_KingCapturesWestAtOccupiedLocation()
        {
            var king = new King(ChessPosition.C5, ChessColor.Colors.Black);
            var board = new Board(new BoardState(ChessPosition.D5), new BoardState(ChessPosition.C5));

            king.GenerateCaptures(board.State, new BoardState(king.Location));

            Assert.IsTrue(king.CanCaptureAt(ChessPosition.D5));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_KingCapturesNorthEastAtOccupiedLocation()
        {
            var king = new King(ChessPosition.C5, ChessColor.Colors.Black);
            var board = new Board(new BoardState(ChessPosition.B6), new BoardState(ChessPosition.C5));

            king.GenerateCaptures(board.State, new BoardState(king.Location));

            Assert.IsTrue(king.CanCaptureAt(ChessPosition.B6));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_KingCapturesNorthWestAtOccupiedLocation()
        {
            var king = new King(ChessPosition.C5, ChessColor.Colors.Black);
            var board = new Board(new BoardState(ChessPosition.D6), new BoardState(ChessPosition.C5));

            king.GenerateCaptures(board.State, new BoardState(king.Location));

            Assert.IsTrue(king.CanCaptureAt(ChessPosition.D6));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_KingCapturesSouthEastAtOccupiedLocation()
        {
            var king = new King(ChessPosition.C5, ChessColor.Colors.Black);
            var board = new Board(new BoardState(ChessPosition.B4), new BoardState(ChessPosition.C5));

            king.GenerateCaptures(board.State, new BoardState(king.Location));

            Assert.IsTrue(king.CanCaptureAt(ChessPosition.B4));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_KingCapturesSouthWestAtOccupiedLocation()
        {
            var king = new King(ChessPosition.C4, ChessColor.Colors.Black);
            var board = new Board(new BoardState(ChessPosition.D3), new BoardState(ChessPosition.C4));

            king.GenerateCaptures(board.State, new BoardState(king.Location));

            Assert.IsTrue(king.CanCaptureAt(ChessPosition.D3));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_KingCapturesNorthAtUnccupiedLocation()
        {
            var king = new King(ChessPosition.C4, ChessColor.Colors.Black);
            var board = new Board(new BoardState(), new BoardState(king.Location));

            king.GenerateCaptures(board.State, new BoardState(king.Location));

            Assert.IsFalse(king.CanCaptureAt(ChessPosition.C5));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_KingCapturesSouthAtUnoccupiedLocation()
        {
            var king = new King(ChessPosition.C4, ChessColor.Colors.Black);
            var board = new Board(new BoardState(), new BoardState(king.Location));

            king.GenerateCaptures(board.State, new BoardState(king.Location));

            Assert.IsFalse(king.CanCaptureAt(ChessPosition.C3));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_KingCapturesEastAtUnoccupiedLocation()
        {
            var king = new King(ChessPosition.C5, ChessColor.Colors.Black);
            var board = new Board(new BoardState(), new BoardState(king.Location));

            king.GenerateCaptures(board.State, new BoardState(king.Location));

            Assert.IsFalse(king.CanCaptureAt(ChessPosition.B5));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_KingCapturesWestAtUnoccupiedLocation()
        {
            var king = new King(ChessPosition.C5, ChessColor.Colors.Black);
            var board = new Board(new BoardState(), new BoardState(king.Location));

            king.GenerateCaptures(board.State, new BoardState(king.Location));

            Assert.IsFalse(king.CanCaptureAt(ChessPosition.D5));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_KingCapturesNorthEastAtUnoccupiedLocation()
        {
            var king = new King(ChessPosition.C5, ChessColor.Colors.Black);
            var board = new Board(new BoardState(), new BoardState(king.Location));

            king.GenerateCaptures(board.State, new BoardState(king.Location));

            Assert.IsFalse(king.CanCaptureAt(ChessPosition.B6));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_KingCapturesNorthWestAtUnoccupiedLocation()
        {
            var king = new King(ChessPosition.C5, ChessColor.Colors.Black);
            var board = new Board(new BoardState(), new BoardState(king.Location));

            king.GenerateCaptures(board.State, new BoardState(king.Location));

            Assert.IsFalse(king.CanCaptureAt(ChessPosition.D6));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_KingCapturesSouthEastAtUnoccupiedLocation()
        {
            var king = new King(ChessPosition.C5, ChessColor.Colors.Black);
            var board = new Board(new BoardState(), new BoardState(king.Location));

            king.GenerateCaptures(board.State, new BoardState(king.Location));

            Assert.IsFalse(king.CanCaptureAt(ChessPosition.B4));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_KingCapturesSouthWestAtUnoccupiedLocation()
        {
            var king = new King(ChessPosition.C4, ChessColor.Colors.Black);
            var board = new Board(new BoardState(), new BoardState(king.Location));

            king.GenerateCaptures(board.State, new BoardState(king.Location));

            Assert.IsFalse(king.CanCaptureAt(ChessPosition.D3));
        }


        #endregion

        #region Threaten Tests

        [TestMethod]
        public void Should_ReturnTrue_For_ThreatenedSquares()
        {
            Piece king = new King(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(king.Location), new BoardState());

            king.GenerateThreatened(board.State, new BoardState(king.Location));

            Assert.IsTrue(king.IsThreateningAt(ChessPosition.D6));
            Assert.IsTrue(king.IsThreateningAt(ChessPosition.C6));
            Assert.IsTrue(king.IsThreateningAt(ChessPosition.B6));

            Assert.IsTrue(king.IsThreateningAt(ChessPosition.B5));
            Assert.IsTrue(king.IsThreateningAt(ChessPosition.D5));

            Assert.IsTrue(king.IsThreateningAt(ChessPosition.D4));
            Assert.IsTrue(king.IsThreateningAt(ChessPosition.C4));
            Assert.IsTrue(king.IsThreateningAt(ChessPosition.B4));
        }

        #endregion
    }
}
