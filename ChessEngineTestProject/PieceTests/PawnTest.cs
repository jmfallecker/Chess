﻿using ChessEngine.Model.BoardModel;
using ChessEngine.Model.ChessUtility;
using ChessEngine.Model.PieceModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChessTestProject.PieceTests
{
    [TestClass]
    public class PawnTest
    {
        #region Movement Tests
        [TestMethod]
        public void Should_ReturnTrue_When_WhitePawnIsTakingOneSpaceFirstMove()
        {
            var board = new Board();
            var pawn = new Pawn(ChessPosition.A2, ChessColor.Colors.White);

            pawn.GenerateMoves(board.State);

            Assert.IsTrue(pawn.CanMoveTo(ChessPosition.A3));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_BlackPawnIsTakingOneSpaceFirstMove()
        {
            var board = new Board();
            var pawn = new Pawn(ChessPosition.A7, ChessColor.Colors.Black);

            pawn.GenerateMoves(board.State);

            Assert.IsTrue(pawn.CanMoveTo(ChessPosition.A6));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_WhitePawnIsMovingOneSpaceAfterFirstMove()
        {
            var board = new Board();
            var pawn = new Pawn(ChessPosition.A2, ChessColor.Colors.White);
            pawn.MoveTo(ChessPosition.A3);

            pawn.GenerateMoves(board.State);
            Assert.IsTrue(pawn.CanMoveTo(ChessPosition.A4));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_BlackPawnIsMovingOneSpaceAfterFirstMove()
        {
            var board = new Board();
            var pawn = new Pawn(ChessPosition.A7, ChessColor.Colors.Black);
            pawn.MoveTo(ChessPosition.A6);

            pawn.GenerateMoves(board.State);
            Assert.IsTrue(pawn.CanMoveTo(ChessPosition.A5));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_WhitePawnIsTakingTwoSpaceFirstMove()
        {
            var board = new Board();
            var pawn = new Pawn(ChessPosition.A2, ChessColor.Colors.White);

            pawn.GenerateMoves(board.State);

            Assert.IsTrue(pawn.CanMoveTo(ChessPosition.A4));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_BlackPawnIsTakingTwoSpaceFirstMove()
        {
            var board = new Board();
            var pawn = new Pawn(ChessPosition.A7, ChessColor.Colors.Black);

            pawn.GenerateMoves(board.State);

            Assert.IsTrue(pawn.CanMoveTo(ChessPosition.A5));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_WhitePawnIsMovingTwoSpacesAfterFirstMove()
        {
            var board = new Board();
            var pawn = new Pawn(ChessPosition.A2, ChessColor.Colors.White);
            pawn.MoveTo(ChessPosition.A3);

            pawn.GenerateMoves(board.State);
            Assert.IsFalse(pawn.CanMoveTo(ChessPosition.A5));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_BlackPawnIsMovingTwoSpacesAfterFirstMove()
        {
            var board = new Board();
            var pawn = new Pawn(ChessPosition.A7, ChessColor.Colors.Black);
            pawn.MoveTo(ChessPosition.A6);

            pawn.GenerateMoves(board.State);
            Assert.IsFalse(pawn.CanMoveTo(ChessPosition.A4));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_WhitePawnMovesTwoFilesInOneMove()
        {
            var pawn = new Pawn(ChessPosition.A2, ChessColor.Colors.White);
            Assert.IsFalse(pawn.CanMoveTo(ChessPosition.C4));

            pawn.MoveTo(ChessPosition.A4);
            Assert.IsFalse(pawn.CanMoveTo(ChessPosition.C4));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_BlackPawnMovesTwoFilesInOneMove()
        {
            var pawn = new Pawn(ChessPosition.A7, ChessColor.Colors.White);
            Assert.IsFalse(pawn.CanMoveTo(ChessPosition.C5));

            pawn.MoveTo(ChessPosition.A5);
            Assert.IsFalse(pawn.CanMoveTo(ChessPosition.C5));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_WhitePawnWouldJumpPiece()
        {
            var board = new Board();
            var pawn = new Pawn(ChessPosition.A2, ChessColor.Colors.White);

            board.Add(ChessPosition.A3);

            Assert.IsFalse(pawn.CanMoveTo(ChessPosition.A4));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_BlackPawnWouldJumpPiece()
        {
            var board = new Board();
            var pawn = new Pawn(ChessPosition.A7, ChessColor.Colors.Black);

            board.Add(ChessPosition.A6);

            Assert.IsFalse(pawn.CanMoveTo(ChessPosition.A5));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_WhitePawnMovingForwardToOccupiedSpace()
        {
            var board = new Board();
            var pawn = new Pawn(ChessPosition.A2, ChessColor.Colors.White);

            board.Add(ChessPosition.A3);

            Assert.IsFalse(pawn.CanMoveTo(ChessPosition.A3));

        }

        [TestMethod]
        public void Should_ReturnFalse_When_BlackPawnMovingForwardToOccupiedSpace()
        {
            var board = new Board();
            var pawn = new Pawn(ChessPosition.A7, ChessColor.Colors.Black);

            board.Add(ChessPosition.A6);

            Assert.IsFalse(pawn.CanMoveTo(ChessPosition.A6));
        }

        [TestMethod]
        public void Should_BeCapturableEnPassant_After_MovingFirstMoveTwoSpacesForward()
        {
            var board = new Board();
            var pawn = new Pawn(ChessPosition.A2, ChessColor.Colors.White);

            board.Add(pawn.Location);

            pawn.MoveTo(ChessPosition.A4);

            Assert.IsTrue(pawn.IsCapturableByEnPassant);
        }

        [TestMethod]
        public void Should_BeNotCapturableEnPassant_After_MovingFirstMoveOneSpacesForward()
        {
            var board = new Board();
            var pawn = new Pawn(ChessPosition.A7, ChessColor.Colors.Black);

            board.Add(pawn.Location);

            pawn.MoveTo(ChessPosition.A6);

            Assert.IsFalse(pawn.IsCapturableByEnPassant);
        }

        [TestMethod]
        public void Should_BeNotCapturableEnPassant_After_MovingFirstMoveTwoSpacesForwardThenMovingAgain()
        {
            var board = new Board();
            var pawn = new Pawn(ChessPosition.A2, ChessColor.Colors.White);

            board.Add(pawn.Location);

            pawn.MoveTo(ChessPosition.A4);

            Assert.IsTrue(pawn.IsCapturableByEnPassant); // true for the first move

            pawn.MoveTo(ChessPosition.A5);

            Assert.IsFalse(pawn.IsCapturableByEnPassant);
        }

        [TestMethod]
        public void Should_BeNotCapturableEnPassant_After_MovingFirstMoveOneSpacesForwardThenOneSpaceAgain()
        {
            var board = new Board();
            var pawn = new Pawn(ChessPosition.A7, ChessColor.Colors.Black);

            board.Add(pawn.Location);

            pawn.MoveTo(ChessPosition.A6);

            Assert.IsFalse(pawn.IsCapturableByEnPassant);

            pawn.MoveTo(ChessPosition.A5);

            Assert.IsFalse(pawn.IsCapturableByEnPassant);
        }

        [TestMethod]
        public void Should_BePromotable_After_MovingAcrossTheBoardWhite()
        {
            var board = new Board();
            var pawn = new Pawn(ChessPosition.A2, ChessColor.Colors.White);

            pawn.MoveTo(ChessPosition.A8);

            Assert.IsTrue(pawn.IsPromotable);
        }

        [TestMethod]
        public void Should_BePromotable_After_MovingAcrossTheBoardBlack()
        {
            var board = new Board();
            var pawn = new Pawn(ChessPosition.A7, ChessColor.Colors.Black);

            pawn.MoveTo(ChessPosition.A1);

            Assert.IsTrue(pawn.IsPromotable);
        }


        #endregion

        #region Capture Tests

        [TestMethod]
        public void Should_ReturnFalse_When_WhitePawnCapturingEastAtUnoccupiedLocation()
        {
            var board = new Board(new BoardState(ChessPosition.WhiteStart), new BoardState());
            var pawn = new Pawn(ChessPosition.C2, ChessColor.Colors.White);

            pawn.GenerateCaptures(board.State, new BoardState(pawn.Location));

            Assert.IsFalse(pawn.CanCaptureAt(ChessPosition.D3));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_WhitePawnCapturingEastAtOccupiedLocation()
        {
            var board = new Board(new BoardState(ChessPosition.WhiteStart), new BoardState(ChessPosition.D3));
            var pawn = new Pawn(ChessPosition.C2, ChessColor.Colors.White);

            pawn.GenerateCaptures(board.State, new BoardState(pawn.Location));
            Assert.IsTrue(pawn.CanCaptureAt(ChessPosition.D3));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_WhitePawnCapturingWestAtUnoccupiedLocation()
        {
            var board = new Board(new BoardState(ChessPosition.WhiteStart), new BoardState());
            var pawn = new Pawn(ChessPosition.C2, ChessColor.Colors.White);

            pawn.GenerateCaptures(board.State, new BoardState(pawn.Location));
            Assert.IsFalse(pawn.CanCaptureAt(ChessPosition.B3));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_WhitePawnCapturingWestAtOccupiedLocation()
        {
            var board = new Board(new BoardState(ChessPosition.WhiteStart), new BoardState(ChessPosition.B3));
            var pawn = new Pawn(ChessPosition.C2, ChessColor.Colors.White);

            pawn.GenerateCaptures(board.State, new BoardState(pawn.Location));
            Assert.IsTrue(pawn.CanCaptureAt(ChessPosition.B3));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_BlackPawnCapturingEastAtUnoccupiedLocation()
        {
            var board = new Board(new BoardState(), new BoardState(ChessPosition.BlackStart));
            var pawn = new Pawn(ChessPosition.C7, ChessColor.Colors.Black);

            pawn.GenerateCaptures(board.State, new BoardState(pawn.Location));
            Assert.IsFalse(pawn.CanCaptureAt(ChessPosition.D6));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_BlackPawnCapturingEastAtOccupiedLocation()
        {
            var board = new Board(new BoardState(ChessPosition.D6), new BoardState(ChessPosition.BlackStart));
            var pawn = new Pawn(ChessPosition.C7, ChessColor.Colors.Black);

            pawn.GenerateCaptures(board.State, new BoardState(pawn.Location));
            Assert.IsTrue(pawn.CanCaptureAt(ChessPosition.D6));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_BlackPawnCapturingWestAtUnoccupiedLocation()
        {
            var board = new Board(new BoardState(), new BoardState(ChessPosition.BlackStart));
            var pawn = new Pawn(ChessPosition.C7, ChessColor.Colors.Black);

            pawn.GenerateCaptures(board.State, new BoardState(pawn.Location));
            Assert.IsFalse(pawn.CanCaptureAt(ChessPosition.B6));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_BlackPawnCapturingWestAtOccupiedLocation()
        {
            var board = new Board(new BoardState(ChessPosition.B6), new BoardState(ChessPosition.BlackStart));
            var pawn = new Pawn(ChessPosition.C7, ChessColor.Colors.Black);

            pawn.GenerateCaptures(board.State, new BoardState(pawn.Location));
            Assert.IsTrue(pawn.CanCaptureAt(ChessPosition.B6));
        }

        #endregion

        #region Threaten Tests

        [TestMethod]
        public void Should_ReturnTrue_For_WhitePawnThreatenedSquares()
        {
            Piece pawn = new Pawn(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(pawn.Location), new BoardState());

            pawn.GenerateThreatened(board.State, new BoardState(pawn.Location));

            Assert.IsTrue(pawn.IsThreateningAt(ChessPosition.D6));
            Assert.IsTrue(pawn.IsThreateningAt(ChessPosition.B6));
        }

        #endregion
    }
}
