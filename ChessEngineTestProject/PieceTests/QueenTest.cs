﻿using ChessEngine.Model.BoardModel;
using ChessEngine.Model.ChessUtility;
using ChessEngine.Model.PieceModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChessTestProject.PieceTests
{
    [TestClass]
    public class QueenTest
    {
        #region Movement Tests

        [TestMethod]
        public void Should_ReturnTrue_When_QueenMovingNorthEast()
        {
            var board = new Board(new BoardState(ChessPosition.None), new BoardState(ChessPosition.None));

            var queen = new Queen(ChessPosition.D4, ChessColor.Colors.Black);

            queen.GenerateMoves(board.State);

            Assert.IsTrue(queen.CanMoveTo(ChessPosition.F6));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_QueenMovingNorthWest()
        {
            var board = new Board(new BoardState(ChessPosition.None), new BoardState(ChessPosition.None));

            var queen = new Queen(ChessPosition.D4, ChessColor.Colors.Black);

            queen.GenerateMoves(board.State);

            Assert.IsTrue(queen.CanMoveTo(ChessPosition.B6));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_QueenMovingSouthEast()
        {
            var board = new Board(new BoardState(ChessPosition.None), new BoardState(ChessPosition.None));

            var queen = new Queen(ChessPosition.D4, ChessColor.Colors.Black);

            queen.GenerateMoves(board.State);

            Assert.IsTrue(queen.CanMoveTo(ChessPosition.F2));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_QueenMovingSouthWest()
        {

            var board = new Board(new BoardState(ChessPosition.None), new BoardState(ChessPosition.None));

            var queen = new Queen(ChessPosition.D4, ChessColor.Colors.Black);

            queen.GenerateMoves(board.State);

            Assert.IsTrue(queen.CanMoveTo(ChessPosition.B2));
        }

        [TestMethod]
        public void Should_ReturnFalse_WhenQueenJumpingPieceNorthEast()
        {

            var board = new Board(new BoardState(ChessPosition.None), new BoardState(ChessPosition.None));

            var queen = new Queen(ChessPosition.D4, ChessColor.Colors.Black);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.E5);

            queen.GenerateMoves(board.State);

            Assert.IsFalse(queen.CanMoveTo(ChessPosition.F6));
        }

        [TestMethod]
        public void Should_ReturnFalse_WhenQueenJumpingPieceNorthWest()
        {

            var board = new Board(new BoardState(ChessPosition.None), new BoardState(ChessPosition.None));

            var queen = new Queen(ChessPosition.D4, ChessColor.Colors.Black);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.C5);

            queen.GenerateMoves(board.State);

            Assert.IsFalse(queen.CanMoveTo(ChessPosition.B6));
        }

        [TestMethod]
        public void Should_ReturnFalse_WhenQueenJumpingPieceSouthEast()
        {

            var board = new Board(new BoardState(), new BoardState());

            var queen = new Queen(ChessPosition.D4, ChessColor.Colors.Black);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.E3);

            queen.GenerateMoves(board.State);

            Assert.IsFalse(queen.CanMoveTo(ChessPosition.F2));
        }

        [TestMethod]
        public void Should_ReturnFalse_WhenQueenJumpingPieceSouthWest()
        {

            var board = new Board(new BoardState(), new BoardState());

            var queen = new Queen(ChessPosition.D4, ChessColor.Colors.Black);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.C3);

            queen.GenerateMoves(board.State);

            Assert.IsFalse(queen.CanMoveTo(ChessPosition.B2));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_QueenIsMovingNorth()
        {

            var board = new Board(new BoardState(), new BoardState());

            var queen = new Queen(ChessPosition.A1, ChessColor.Colors.White);
            queen.GenerateMoves(board.State);

            Assert.IsTrue(queen.CanMoveTo(ChessPosition.A8));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_QueenIsMovingSouth()
        {

            var board = new Board(new BoardState(), new BoardState());

            var queen = new Queen(ChessPosition.A8, ChessColor.Colors.White);
            queen.GenerateMoves(board.State);

            Assert.IsTrue(queen.CanMoveTo(ChessPosition.A1));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_QueenIsMovingEast()
        {

            var board = new Board(new BoardState(), new BoardState());

            var queen = new Queen(ChessPosition.A1, ChessColor.Colors.White);
            queen.GenerateMoves(board.State);

            Assert.IsTrue(queen.CanMoveTo(ChessPosition.H1));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_QueenIsMovingWest()
        {

            var board = new Board(new BoardState(), new BoardState());

            var queen = new Queen(ChessPosition.H1, ChessColor.Colors.White);
            queen.GenerateMoves(board.State);

            Assert.IsTrue(queen.CanMoveTo(ChessPosition.A1));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_QueenIsJumpingPieceNorth()
        {

            var board = new Board(new BoardState(), new BoardState());

            var queen = new Queen(ChessPosition.A1, ChessColor.Colors.White);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.A4);

            queen.GenerateMoves(board.State);

            Assert.IsFalse(queen.CanMoveTo(ChessPosition.A8));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_QueenIsJumpingPieceSouth()
        {

            var board = new Board(new BoardState(), new BoardState());

            var queen = new Queen(ChessPosition.A8, ChessColor.Colors.White);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.A4);

            queen.GenerateMoves(board.State);

            Assert.IsFalse(queen.CanMoveTo(ChessPosition.A1));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_QueenIsJumpingPieceEast()
        {

            var board = new Board(new BoardState(), new BoardState());

            var queen = new Queen(ChessPosition.A1, ChessColor.Colors.White);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.D1);

            queen.GenerateMoves(board.State);

            Assert.IsFalse(queen.CanMoveTo(ChessPosition.H1));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_QueenIsJumpingPieceWest()
        {

            var board = new Board(new BoardState(), new BoardState());

            var queen = new Queen(ChessPosition.H1, ChessColor.Colors.White);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.D1);

            queen.GenerateMoves(board.State);

            Assert.IsFalse(queen.CanMoveTo(ChessPosition.A1));
        }

        #endregion

        #region Capture Tests

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingNorthEastAtUnoccupiedLocation()
        {

            Piece piece = new Queen(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(piece.Location), new BoardState());


            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsFalse(piece.CanCaptureAt(ChessPosition.F8));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingNorthWestAtUnoccupiedLocation()
        {

            Piece piece = new Queen(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(piece.Location), new BoardState());


            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsFalse(piece.CanCaptureAt(ChessPosition.A7));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingSouthEastAtUnoccupiedLocation()
        {

            Piece piece = new Queen(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(piece.Location), new BoardState());


            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsFalse(piece.CanCaptureAt(ChessPosition.G1));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingSouthWestAtUnoccupiedLocation()
        {

            Piece piece = new Queen(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(piece.Location), new BoardState());


            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsFalse(piece.CanCaptureAt(ChessPosition.A3));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingNorthEastAtOccupiedLocation()
        {

            Piece piece = new Queen(ChessPosition.C5, ChessColor.Colors.White);
            var enemyPiecePosition = ChessPosition.F8;

            var board = new Board(new BoardState(piece.Location), new BoardState(enemyPiecePosition));


            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsTrue(piece.CanCaptureAt(enemyPiecePosition));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingNorthWestAtOccupiedLocation()
        {

            Piece piece = new Queen(ChessPosition.C5, ChessColor.Colors.White);
            var enemyPiecePosition = ChessPosition.A7;

            var board = new Board(new BoardState(piece.Location), new BoardState(enemyPiecePosition));


            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsTrue(piece.CanCaptureAt(enemyPiecePosition));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingSouthEastAtOccupiedLocation()
        {

            Piece piece = new Queen(ChessPosition.C5, ChessColor.Colors.White);
            var enemyPiecePosition = ChessPosition.G1;

            var board = new Board(new BoardState(piece.Location), new BoardState(enemyPiecePosition));


            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsTrue(piece.CanCaptureAt(enemyPiecePosition));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingSouthWestAtOccupiedLocation()
        {

            Piece piece = new Queen(ChessPosition.C5, ChessColor.Colors.White);
            var enemyPiecePosition = ChessPosition.A3;

            var board = new Board(new BoardState(piece.Location), new BoardState(enemyPiecePosition));


            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsTrue(piece.CanCaptureAt(enemyPiecePosition));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingNorthAtOccupiedLocation()
        {

            var enemyPiecePosition = ChessPosition.C8;
            Piece piece = new Queen(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(piece.Location), new BoardState(enemyPiecePosition));


            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsTrue(piece.CanCaptureAt(enemyPiecePosition));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingSouthAtOccupiedLocation()
        {

            var enemyPiecePosition = ChessPosition.C1;
            Piece piece = new Queen(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(piece.Location), new BoardState(enemyPiecePosition));


            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsTrue(piece.CanCaptureAt(enemyPiecePosition));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingEastAtOccupiedLocation()
        {

            var enemyPiecePosition = ChessPosition.H5;
            Piece piece = new Queen(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(piece.Location), new BoardState(enemyPiecePosition));


            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsTrue(piece.CanCaptureAt(enemyPiecePosition));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingWestAtOccupiedLocation()
        {

            var enemyPiecePosition = ChessPosition.A5;
            Piece piece = new Queen(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(piece.Location), new BoardState(enemyPiecePosition));


            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsTrue(piece.CanCaptureAt(enemyPiecePosition));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingNorthAtUnoccupiedLocation()
        {

            var enemyPieceLocation = ChessPosition.C8;
            Piece piece = new Queen(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(piece.Location), new BoardState());


            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsFalse(piece.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingSouthAtUnoccupiedLocation()
        {

            var enemyPieceLocation = ChessPosition.C1;
            Piece piece = new Queen(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(piece.Location), new BoardState());


            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsFalse(piece.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingEastAtUnoccupiedLocation()
        {

            var enemyPieceLocation = ChessPosition.H5;
            Piece piece = new Queen(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(piece.Location), new BoardState());


            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsFalse(piece.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingWestAtUnoccupiedLocation()
        {

            var enemyPieceLocation = ChessPosition.A5;
            Piece piece = new Queen(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(piece.Location), new BoardState());


            piece.GenerateCaptures(board.State, new BoardState(piece.Location));

            Assert.IsFalse(piece.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingNorthEastAtOccupiedLocationWithFriendliesInBetween()
        {
            var enemyPieceLocation = ChessPosition.H8;
            var enemyPieceLocations = new BoardState
            {
                enemyPieceLocation
            };

            Piece queen = new Queen(ChessPosition.E5, ChessColor.Colors.Black);
            var friendlyPieceLocation = new BoardState
            {
                queen.Location,
                ChessPosition.F6,
                ChessPosition.G7
            };

            var board = new Board(enemyPieceLocations, friendlyPieceLocation);
            queen.GenerateCaptures(board.State, friendlyPieceLocation);

            Assert.IsFalse(queen.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingNorthWestAtOccupiedLocationWithFriendliesInBetween()
        {
            var enemyPieceLocation = ChessPosition.B8;
            var enemyPieceLocations = new BoardState
            {
                enemyPieceLocation
            };

            Piece queen = new Queen(ChessPosition.E5, ChessColor.Colors.Black);
            var friendlyPieceLocation = new BoardState
            {
                queen.Location,
                ChessPosition.D6,
                ChessPosition.C7
            };

            var board = new Board(enemyPieceLocations, friendlyPieceLocation);
            queen.GenerateCaptures(board.State, friendlyPieceLocation);

            Assert.IsFalse(queen.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingSouthEastAtOccupiedLocationWithFriendliesInBetween()
        {
            var enemyPieceLocation = ChessPosition.H1;
            var enemyPieceLocations = new BoardState
            {
                enemyPieceLocation
            };

            Piece queen = new Queen(ChessPosition.E4, ChessColor.Colors.Black);
            var friendlyPieceLocation = new BoardState
            {
                queen.Location,
                ChessPosition.F3,
                ChessPosition.G2
            };

            var board = new Board(enemyPieceLocations, friendlyPieceLocation);
            queen.GenerateCaptures(board.State, friendlyPieceLocation);

            Assert.IsFalse(queen.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingSouthWestAtOccupiedLocationWithFriendliesInBetween()
        {
            var enemyPieceLocation = ChessPosition.B1;
            var enemyPieceLocations = new BoardState
            {
                enemyPieceLocation
            };

            Piece queen = new Queen(ChessPosition.E4, ChessColor.Colors.Black);
            var friendlyPieceLocation = new BoardState
            {
                queen.Location,
                ChessPosition.D3,
                ChessPosition.C2
            };

            var board = new Board(enemyPieceLocations, friendlyPieceLocation);
            queen.GenerateCaptures(board.State, friendlyPieceLocation);

            Assert.IsFalse(queen.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingNorthAtOccupiedLocationWithFriendliesInBetween()
        {
            var enemyPieceLocation = ChessPosition.H8;
            var enemyPieceLocations = new BoardState
            {
                enemyPieceLocation
            };

            Piece queen = new Queen(ChessPosition.H5, ChessColor.Colors.Black);
            var friendlyPieceLocation = new BoardState
            {
                queen.Location,
                ChessPosition.H6,
                ChessPosition.H7
            };

            var board = new Board(enemyPieceLocations, friendlyPieceLocation);
            queen.GenerateCaptures(board.State, friendlyPieceLocation);

            Assert.IsFalse(queen.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingSouthAtOccupiedLocationWithFriendliesInBetween()
        {
            var enemyPieceLocation = ChessPosition.H2;
            var enemyPieceLocations = new BoardState
            {
                enemyPieceLocation
            };

            Piece queen = new Queen(ChessPosition.H5, ChessColor.Colors.Black);
            var friendlyPieceLocation = new BoardState
            {
                queen.Location,
                ChessPosition.H4,
                ChessPosition.H3
            };

            var board = new Board(enemyPieceLocations, friendlyPieceLocation);
            queen.GenerateCaptures(board.State, friendlyPieceLocation);

            Assert.IsFalse(queen.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingEastAtOccupiedLocationWithFriendliesInBetween()
        {
            var enemyPieceLocation = ChessPosition.H5;
            var enemyPieceLocations = new BoardState
            {
                enemyPieceLocation
            };

            Piece queen = new Queen(ChessPosition.F5, ChessColor.Colors.Black);
            var friendlyPieceLocation = new BoardState
            {
                queen.Location,
                ChessPosition.G5
            };

            var board = new Board(enemyPieceLocations, friendlyPieceLocation);
            queen.GenerateCaptures(board.State, friendlyPieceLocation);

            Assert.IsFalse(queen.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingWestAtOccupiedLocationWithFriendliesInBetween()
        {
            var enemyPieceLocation = ChessPosition.F5;
            var enemyPieceLocations = new BoardState
            {
                enemyPieceLocation
            };

            Piece queen = new Queen(ChessPosition.H5, ChessColor.Colors.Black);
            var friendlyPieceLocation = new BoardState
            {
                queen.Location,
                ChessPosition.G5
            };

            var board = new Board(enemyPieceLocations, friendlyPieceLocation);
            queen.GenerateCaptures(board.State, friendlyPieceLocation);

            Assert.IsFalse(queen.CanCaptureAt(enemyPieceLocation));
        }

        #endregion

        #region Threaten Tests

        [TestMethod]
        public void Should_ReturnTrue_For_ThreatenedSquares()
        {
            Piece piece = new Queen(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(piece.Location), new BoardState());

            piece.GenerateThreatened(board.State, new BoardState(piece.Location));

            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.A5));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.B5));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.D5));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.E5));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.F5));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.G5));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.H5));

            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.C1));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.C2));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.C3));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.C4));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.C6));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.C7));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.C8));

            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.D6));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.E7));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.F8));

            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.B4));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.A3));

            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.B6));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.A7));

            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.D4));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.E3));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.F2));
            Assert.IsTrue(piece.IsThreateningAt(ChessPosition.G1));
        }

        #endregion
    }
}
