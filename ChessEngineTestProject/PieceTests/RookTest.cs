﻿using ChessEngine.Model.BoardModel;
using ChessEngine.Model.ChessUtility;
using ChessEngine.Model.PieceModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChessTestProject.PieceTests
{
    [TestClass]
    public class RookTest
    {
        #region Movement Tests

        [TestMethod]
        public void Should_ReturnTrue_When_RookIsMovingNorth()
        {
            var board = new Board(new BoardState(), new BoardState());

            var rook = new Rook(ChessPosition.A1, ChessColor.Colors.White);
            rook.GenerateMoves(board.State);

            Assert.IsTrue(rook.CanMoveTo(ChessPosition.A8));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_RookIsMovingSouth()
        {
            var board = new Board(new BoardState(), new BoardState());

            var rook = new Rook(ChessPosition.A8, ChessColor.Colors.White);
            rook.GenerateMoves(board.State);

            Assert.IsTrue(rook.CanMoveTo(ChessPosition.A1));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_RookIsMovingEast()
        {
            var board = new Board(new BoardState(), new BoardState());

            var rook = new Rook(ChessPosition.A1, ChessColor.Colors.White);
            rook.GenerateMoves(board.State);

            Assert.IsTrue(rook.CanMoveTo(ChessPosition.H1));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_RookIsMovingWest()
        {
            var board = new Board(new BoardState(), new BoardState());

            var rook = new Rook(ChessPosition.H1, ChessColor.Colors.White);
            rook.GenerateMoves(board.State);

            Assert.IsTrue(rook.CanMoveTo(ChessPosition.A1));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_RookIsJumpingPieceNorth()
        {
            var board = new Board(new BoardState(), new BoardState());

            var rook = new Rook(ChessPosition.A1, ChessColor.Colors.White);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.A4);

            rook.GenerateMoves(board.State);

            Assert.IsFalse(rook.CanMoveTo(ChessPosition.A8));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_RookIsJumpingPieceSouth()
        {
            var board = new Board(new BoardState(), new BoardState());

            var rook = new Rook(ChessPosition.A8, ChessColor.Colors.White);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.A4);

            rook.GenerateMoves(board.State);

            Assert.IsFalse(rook.CanMoveTo(ChessPosition.A1));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_RookIsJumpingPieceEast()
        {
            var board = new Board(new BoardState(), new BoardState());

            var rook = new Rook(ChessPosition.A1, ChessColor.Colors.White);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.D1);

            rook.GenerateMoves(board.State);

            Assert.IsFalse(rook.CanMoveTo(ChessPosition.H1));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_RookIsJumpingPieceWest()
        {
            var board = new Board(new BoardState(), new BoardState());

            var rook = new Rook(ChessPosition.H1, ChessColor.Colors.White);

            // Place a piece between intial square and destination
            board.Add(ChessPosition.D1);

            rook.GenerateMoves(board.State);

            Assert.IsFalse(rook.CanMoveTo(ChessPosition.A1));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_RookIsMovingNorthEast()
        {
            var board = new Board(new BoardState(), new BoardState());

            var rook = new Rook(ChessPosition.D4, ChessColor.Colors.White);
            rook.GenerateMoves(board.State);

            Assert.IsFalse(rook.CanMoveTo(ChessPosition.F6));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_RookIsMovingNorthWest()
        {
            var board = new Board(new BoardState(), new BoardState());

            var rook = new Rook(ChessPosition.D4, ChessColor.Colors.White);
            rook.GenerateMoves(board.State);

            Assert.IsFalse(rook.CanMoveTo(ChessPosition.B6));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_RookIsMovingSouthEast()
        {
            var board = new Board(new BoardState(), new BoardState());

            var rook = new Rook(ChessPosition.D4, ChessColor.Colors.White);
            rook.GenerateMoves(board.State);

            Assert.IsFalse(rook.CanMoveTo(ChessPosition.F2));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_RookIsMovingSouthWest()
        {
            var board = new Board(new BoardState(), new BoardState());

            var rook = new Rook(ChessPosition.D4, ChessColor.Colors.White);
            rook.GenerateMoves(board.State);

            Assert.IsFalse(rook.CanMoveTo(ChessPosition.B2));
        }

        #endregion

        #region Capture Tests

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingNorthAtOccupiedLocation()
        {
            var enemyPieceLocation = ChessPosition.C8;
            Piece rook = new Rook(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(rook.Location), new BoardState(enemyPieceLocation));


            rook.GenerateCaptures(board.State, new BoardState(rook.Location));

            Assert.IsTrue(rook.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingSouthAtOccupiedLocation()
        {
            var enemyPieceLocation = ChessPosition.C1;
            Piece rook = new Rook(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(rook.Location), new BoardState(enemyPieceLocation));


            rook.GenerateCaptures(board.State, new BoardState(rook.Location));

            Assert.IsTrue(rook.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingEastAtOccupiedLocation()
        {
            var enemyPieceLocation = ChessPosition.H5;
            Piece rook = new Rook(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(rook.Location), new BoardState(enemyPieceLocation));


            rook.GenerateCaptures(board.State, new BoardState(rook.Location));

            Assert.IsTrue(rook.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_CapturingWestAtOccupiedLocation()
        {
            var enemyPieceLocation = ChessPosition.A5;
            Piece rook = new Rook(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(rook.Location), new BoardState(enemyPieceLocation));


            rook.GenerateCaptures(board.State, new BoardState(rook.Location));

            Assert.IsTrue(rook.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingNorthAtOccupiedLocationWithFriendliesInBetween()
        {
            var enemyPieceLocation = ChessPosition.H8;
            var enemyPieceLocations = new BoardState
            {
                enemyPieceLocation
            };

            Piece rook = new Rook(ChessPosition.H5, ChessColor.Colors.Black);
            var friendlyPieceLocation = new BoardState
            {
                rook.Location,
                ChessPosition.H6,
                ChessPosition.H7
            };

            var board = new Board(enemyPieceLocations, friendlyPieceLocation);
            rook.GenerateCaptures(board.State, friendlyPieceLocation);

            Assert.IsFalse(rook.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingSouthAtOccupiedLocationWithFriendliesInBetween()
        {
            var enemyPieceLocation = ChessPosition.H2;
            var enemyPieceLocations = new BoardState
            {
                enemyPieceLocation
            };

            Piece rook = new Rook(ChessPosition.H5, ChessColor.Colors.Black);
            var friendlyPieceLocation = new BoardState
            {
                rook.Location,
                ChessPosition.H4,
                ChessPosition.H3
            };

            var board = new Board(enemyPieceLocations, friendlyPieceLocation);
            rook.GenerateCaptures(board.State, friendlyPieceLocation);

            Assert.IsFalse(rook.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingEastAtOccupiedLocationWithFriendliesInBetween()
        {
            var enemyPieceLocation = ChessPosition.H5;
            var enemyPieceLocations = new BoardState
            {
                enemyPieceLocation
            };

            Piece rook = new Rook(ChessPosition.F5, ChessColor.Colors.Black);
            var friendlyPieceLocation = new BoardState
            {
                rook.Location,
                ChessPosition.G5
            };

            var board = new Board(enemyPieceLocations, friendlyPieceLocation);
            rook.GenerateCaptures(board.State, friendlyPieceLocation);

            Assert.IsFalse(rook.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingWestAtOccupiedLocationWithFriendliesInBetween()
        {
            var enemyPieceLocation = ChessPosition.F5;
            var enemyPieceLocations = new BoardState
            {
                enemyPieceLocation
            };

            Piece rook = new Rook(ChessPosition.H5, ChessColor.Colors.Black);
            var friendlyPieceLocation = new BoardState
            {
                rook.Location,
                ChessPosition.G5
            };

            var board = new Board(enemyPieceLocations, friendlyPieceLocation);
            rook.GenerateCaptures(board.State, friendlyPieceLocation);

            Assert.IsFalse(rook.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingNorthAtUnoccupiedLocation()
        {
            var enemyPieceLocation = ChessPosition.C8;
            Piece rook = new Rook(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(rook.Location), new BoardState());


            rook.GenerateCaptures(board.State, new BoardState(rook.Location));

            Assert.IsFalse(rook.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingSouthAtUnoccupiedLocation()
        {
            var enemyPieceLocation = ChessPosition.C1;
            Piece rook = new Rook(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(rook.Location), new BoardState());


            rook.GenerateCaptures(board.State, new BoardState(rook.Location));

            Assert.IsFalse(rook.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingEastAtUnoccupiedLocation()
        {
            var enemyPieceLocation = ChessPosition.H5;
            Piece rook = new Rook(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(rook.Location), new BoardState());


            rook.GenerateCaptures(board.State, new BoardState(rook.Location));

            Assert.IsFalse(rook.CanCaptureAt(enemyPieceLocation));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_CapturingWestAtUnoccupiedLocation()
        {
            var enemyPieceLocation = ChessPosition.A5;
            Piece rook = new Rook(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(rook.Location), new BoardState());


            rook.GenerateCaptures(board.State, new BoardState(rook.Location));

            Assert.IsFalse(rook.CanCaptureAt(enemyPieceLocation));
        }

        #endregion

        #region Threaten Tests

        [TestMethod]
        public void Should_ReturnTrue_For_ThreatenedSquares()
        {
            Piece rook = new Rook(ChessPosition.C5, ChessColor.Colors.White);
            var board = new Board(new BoardState(rook.Location), new BoardState());

            rook.GenerateThreatened(board.State, new BoardState(rook.Location));

            Assert.IsTrue(rook.IsThreateningAt(ChessPosition.A5));
            Assert.IsTrue(rook.IsThreateningAt(ChessPosition.B5));
            Assert.IsTrue(rook.IsThreateningAt(ChessPosition.D5));
            Assert.IsTrue(rook.IsThreateningAt(ChessPosition.E5));
            Assert.IsTrue(rook.IsThreateningAt(ChessPosition.F5));
            Assert.IsTrue(rook.IsThreateningAt(ChessPosition.G5));
            Assert.IsTrue(rook.IsThreateningAt(ChessPosition.H5));

            Assert.IsTrue(rook.IsThreateningAt(ChessPosition.C1));
            Assert.IsTrue(rook.IsThreateningAt(ChessPosition.C2));
            Assert.IsTrue(rook.IsThreateningAt(ChessPosition.C3));
            Assert.IsTrue(rook.IsThreateningAt(ChessPosition.C4));
            Assert.IsTrue(rook.IsThreateningAt(ChessPosition.C6));
            Assert.IsTrue(rook.IsThreateningAt(ChessPosition.C7));
            Assert.IsTrue(rook.IsThreateningAt(ChessPosition.C8));
        }

        #endregion
    }
}
