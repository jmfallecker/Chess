﻿using ChessEngine.Model.ChessUtility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace ChessTestProject.ChessUtilityTests.ChessPositionTests
{
    [TestClass]
    public class ChessPositionUtilityTest
    {
        [TestMethod]
        public void Should_ReturnTrue_If_ListIsSorted()
        {
            List<ChessPosition> list = new List<ChessPosition>
            {
                ChessPosition.H8,
                ChessPosition.G7,
                ChessPosition.F6,
                ChessPosition.E5,
                ChessPosition.D4,
                ChessPosition.C3,
                ChessPosition.B2,
                ChessPosition.A1
            };

            list.Sort(ChessPositionUtility.Comparison);
            bool isListSorted = list[0] == ChessPosition.A1 &&
                                list[1] == ChessPosition.B2 &&
                                list[2] == ChessPosition.C3 &&
                                list[3] == ChessPosition.D4 &&
                                list[4] == ChessPosition.E5 &&
                                list[5] == ChessPosition.F6 &&
                                list[6] == ChessPosition.G7 &&
                                list[7] == ChessPosition.H8;

            Assert.IsTrue(isListSorted);
        }
    }
}
