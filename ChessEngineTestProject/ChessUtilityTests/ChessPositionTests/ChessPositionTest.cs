﻿using ChessEngine.Model.ChessUtility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChessTestProject.ChessUtilityTests.ChessPositionTests
{
    [TestClass]
    public class ChessPositionTest
    {
        [TestMethod]
        public void Should_BeEqual_When_StartingPositionsComparedWithIndividualSquares()
        {


            ulong white = 0L;

            white |= (ulong)ChessPosition.A1;
            white |= (ulong)ChessPosition.B1;
            white |= (ulong)ChessPosition.C1;
            white |= (ulong)ChessPosition.D1;
            white |= (ulong)ChessPosition.E1;
            white |= (ulong)ChessPosition.F1;
            white |= (ulong)ChessPosition.G1;
            white |= (ulong)ChessPosition.H1;

            white |= (ulong)ChessPosition.A2;
            white |= (ulong)ChessPosition.B2;
            white |= (ulong)ChessPosition.C2;
            white |= (ulong)ChessPosition.D2;
            white |= (ulong)ChessPosition.E2;
            white |= (ulong)ChessPosition.F2;
            white |= (ulong)ChessPosition.G2;
            white |= (ulong)ChessPosition.H2;

            Assert.AreEqual((ulong)ChessPosition.WhiteStart, white);

            ulong black = 0L;

            black |= (ulong)ChessPosition.A8;
            black |= (ulong)ChessPosition.B8;
            black |= (ulong)ChessPosition.C8;
            black |= (ulong)ChessPosition.D8;
            black |= (ulong)ChessPosition.E8;
            black |= (ulong)ChessPosition.F8;
            black |= (ulong)ChessPosition.G8;
            black |= (ulong)ChessPosition.H8;

            black |= (ulong)ChessPosition.A7;
            black |= (ulong)ChessPosition.B7;
            black |= (ulong)ChessPosition.C7;
            black |= (ulong)ChessPosition.D7;
            black |= (ulong)ChessPosition.E7;
            black |= (ulong)ChessPosition.F7;
            black |= (ulong)ChessPosition.G7;
            black |= (ulong)ChessPosition.H7;

            Assert.AreEqual((ulong)ChessPosition.BlackStart, black);
        }

        [TestMethod]
        public void Should_BeCorrectValue_For_Rank5()
        {
            ChessPosition position = ChessPosition.A5 |
                                     ChessPosition.B5 |
                                     ChessPosition.C5 |
                                     ChessPosition.D5 |
                                     ChessPosition.E5 |
                                     ChessPosition.F5 |
                                     ChessPosition.G5 |
                                     ChessPosition.H5;

            Assert.AreEqual(position, ChessPosition.Rank5);
        }

        [TestMethod]
        public void Should_BeCorrectValue_For_Rank4()
        {
            ChessPosition position = ChessPosition.A4 |
                                     ChessPosition.B4 |
                                     ChessPosition.C4 |
                                     ChessPosition.D4 |
                                     ChessPosition.E4 |
                                     ChessPosition.F4 |
                                     ChessPosition.G4 |
                                     ChessPosition.H4;

            Assert.AreEqual(position, ChessPosition.Rank4);
        }

        [TestMethod]
        public void Should_BeCorrectValue_For_Rank8()
        {
            ChessPosition position = ChessPosition.A8 |
                                     ChessPosition.B8 |
                                     ChessPosition.C8 |
                                     ChessPosition.D8 |
                                     ChessPosition.E8 |
                                     ChessPosition.F8 |
                                     ChessPosition.G8 |
                                     ChessPosition.H8;

            Assert.AreEqual(position, ChessPosition.Rank8);
        }

        [TestMethod]
        public void Should_BeCorrectValue_For_Rank1()
        {
            ChessPosition position = ChessPosition.A1 |
                                     ChessPosition.B1 |
                                     ChessPosition.C1 |
                                     ChessPosition.D1 |
                                     ChessPosition.E1 |
                                     ChessPosition.F1 |
                                     ChessPosition.G1 |
                                     ChessPosition.H1;

            Assert.AreEqual(position, ChessPosition.Rank1);
        }
    }
}
