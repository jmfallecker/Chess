﻿using ChessEngine.Model.BoardModel;
using ChessEngine.Model.ChessUtility;
using ChessEngine.Model.PieceModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChessTestProject.BoardTests
{
    [TestClass]
    public class BoardTest
    {
        [TestMethod]
        public void Should_ReturnTrue_When_LocationIsOccupied()
        {
            var board = new Board();
            Assert.IsTrue(board.IsPositionOccupied(ChessPosition.A1));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_LocationIsNotOccupied()
        {
            var board = new Board();
            Assert.IsFalse(board.IsPositionOccupied(ChessPosition.A3));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_AddingPieceToOccupiedLocation()
        {
            var board = new Board();
            Piece p = new Pawn(ChessPosition.A2, ChessColor.Colors.White);

            Assert.IsFalse(board.Add(p.Location));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_AddingPieceToUnoccupiedLocation()
        {
            var board = new Board();
            Piece p = new Pawn(ChessPosition.A3, ChessColor.Colors.White);

            Assert.IsTrue(board.Add(p.Location));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_RemovingPieceFromOccupiedLocation()
        {
            var board = new Board();
            Piece p = new Pawn(ChessPosition.A2, ChessColor.Colors.White);

            Assert.IsTrue(board.Remove(p.Location));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_RemovingPieceFromUnoccupiedLocation()
        {
            var board = new Board();

            Assert.IsFalse(board.Remove(ChessPosition.A3));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_RemovingPieceAndCheckingIsOccupied()
        {
            var board = new Board();
            Piece p = new Pawn(ChessPosition.A2, ChessColor.Colors.White);

            Assert.IsTrue(board.IsPositionOccupied(p.Location));

            board.Remove(p.Location);
            Assert.IsFalse(board.IsPositionOccupied(p.Location));
        }

        //[TestMethod]
        //public void Should_UpdateBoardCorrectly_When_MakingAMoveToEmptyTile()
        //{
        //    var boardStartingPosition = ChessPosition.A2;
        //    var boardEndingPosition = ChessPosition.A4;

        //    var board = new Board(new BoardState(boardStartingPosition), new BoardState());
        //    var boardAfterMove = new Board(new BoardState(boardEndingPosition), new BoardState());

        //    var move = new Move
        //    {
        //        StartingPosition = ChessPosition.A2,
        //        EndingPosition = ChessPosition.A4
        //    };

        //    board.ExecuteMove(move);

        //    Assert.AreEqual(boardAfterMove.State, board.State);
        //}

        //[TestMethod]
        //public void Should_NotUpdateBoard_When_MovingToOccupiedSpace()
        //{
        //    var boardStartingPosition = ChessPosition.A2 | ChessPosition.A4;
        //    var boardEndingPosition = ChessPosition.A2 | ChessPosition.A4;

        //    var board = new Board(new BoardState(boardStartingPosition), new BoardState());
        //    var boardAfterMove = new Board(new BoardState(boardEndingPosition), new BoardState());

        //    var move = new Move
        //    {
        //        StartingPosition = ChessPosition.A2,
        //        EndingPosition = ChessPosition.A4
        //    };

        //    board.ExecuteMove(move);

        //    Assert.AreEqual(boardAfterMove.State, board.State);
        //}

        //[TestMethod]
        //public void Should_UpdateBoard_When_CapturingToOccupiedSpace()
        //{
        //    var boardStartingPosition = ChessPosition.A2 | ChessPosition.A4;
        //    var boardEndingPosition = ChessPosition.A4;

        //    var board = new Board(new BoardState(boardStartingPosition), new BoardState());
        //    var boardAfterMove = new Board(new BoardState(boardEndingPosition), new BoardState());

        //    var move = new Capture
        //    {
        //        StartingPosition = ChessPosition.A2,
        //        EndingPosition = ChessPosition.A4
        //    };

        //    board.ExecuteMove(move);

        //    Assert.AreEqual(boardAfterMove.State, board.State);
        //}

        //[TestMethod]
        //public void Should_UpdateBoard_When_CapturingToUnOccupiedSpace()
        //{
        //    var boardStartingPosition = ChessPosition.A2;
        //    var boardEndingPosition = ChessPosition.A4;

        //    var board = new Board(new BoardState(boardStartingPosition), new BoardState());
        //    var boardAfterMove = new Board(new BoardState(boardEndingPosition), new BoardState());

        //    var move = new Capture
        //    {
        //        StartingPosition = ChessPosition.A2,
        //        EndingPosition = ChessPosition.A4
        //    };

        //    board.ExecuteMove(move);

        //    Assert.AreNotEqual(boardAfterMove.State, board.State);
        //}
    }
}
