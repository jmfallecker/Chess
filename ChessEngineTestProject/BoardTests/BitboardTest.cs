﻿using ChessEngine.Model.BoardModel;
using ChessEngine.Model.ChessUtility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ChessTestProject.BoardTests
{
    [TestClass]
    public class BitboardTest
    {
        [TestMethod]
        public void Should_CreateBlankBoard_When_Constructed()
        {
            var bitBoard = new Bitboard();

            var hasPiece = false;

            foreach (ChessPosition position in Enum.GetValues(typeof(ChessPosition)))
            {
                string positionName = Enum.GetName(typeof(ChessPosition), position);
                if (bitBoard.IsLocationOccupied(position) && !positionName.Equals(nameof(ChessPosition.None)))
                {
                    hasPiece = true;
                }
            }

            Assert.IsFalse(hasPiece);
        }

        [TestMethod]
        public void Should_AddPieceToBoard_When_PieceIsAdded()
        {
            var bitBoard = new Bitboard();
            bitBoard.AddPieceToBoard(ChessPosition.A1);

            Assert.IsTrue(bitBoard.IsLocationOccupied(ChessPosition.A1));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_AddingPieceToOccupiedLocation()
        {
            var bitBoard = new Bitboard();
            bitBoard.AddPieceToBoard(ChessPosition.A1);

            Assert.IsFalse(bitBoard.AddPieceToBoard(ChessPosition.A1));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_PositionIsOccupied()
        {
            var bitBoard = new Bitboard();
            var position = ChessPosition.A1;

            bitBoard.AddPieceToBoard(position);

            Assert.IsTrue(bitBoard.IsLocationOccupied(position));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_PositionIsNotOccupied()
        {
            var bitBoard = new Bitboard();
            var position = ChessPosition.A1;

            Assert.IsFalse(bitBoard.IsLocationOccupied(position));
        }

        [TestMethod]
        public void Should_ReturnTrue_When_PieceRemovedFromBoard()
        {
            var bitBoard = new Bitboard();
            var position = ChessPosition.A1;

            bitBoard.AddPieceToBoard(position);

            Assert.IsTrue(bitBoard.RemovePieceFromBoard(position));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_PieceRemovedFromBoard()
        {
            var bitBoard = new Bitboard();
            var position = ChessPosition.A1;

            Assert.IsFalse(bitBoard.RemovePieceFromBoard(position));
        }
    }
}
