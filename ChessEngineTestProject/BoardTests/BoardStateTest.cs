﻿using ChessEngine.Model.BoardModel;
using ChessEngine.Model.ChessUtility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChessTestProject.BoardTests
{
    [TestClass]
    public class BoardStateTest
    {
        [TestMethod]
        public void Should_CreateBoardState_With_SpecifiedPosition()
        {
            BoardState boardState = new BoardState(ChessPosition.A1);

            Assert.IsTrue(boardState.Contains(ChessPosition.A1));
        }

        [TestMethod]
        public void Should_AddPosition_To_OccupiedSquares()
        {
            BoardState boardState = new BoardState();
            boardState.Add(ChessPosition.A1);

            Assert.IsTrue(boardState.Contains(ChessPosition.A1));
        }

        [TestMethod]
        public void Should_RemovePosition_From_OccupiedSquares()
        {
            BoardState boardState = new BoardState();
            boardState.Add(ChessPosition.A1);
            boardState.Remove(ChessPosition.A1);

            Assert.IsFalse(boardState.Contains(ChessPosition.A1));
        }

        [TestMethod]
        public void Should_ReturnTrue_For_OccupiedLocation()
        {
            BoardState boardState = new BoardState();
            boardState.Add(ChessPosition.A1);

            Assert.IsTrue(boardState.IsPositionOccupied(ChessPosition.A1));
        }

        [TestMethod]
        public void Should_ReturnFalse_For_UnoccupiedLocation()
        {
            BoardState boardState = new BoardState();

            Assert.IsFalse(boardState.IsPositionOccupied(ChessPosition.A1));
        }

        [TestMethod]
        public void Should_ReturnTrue_For_EqualBoardStates()
        {
            BoardState boardState = new BoardState();
            BoardState boardState2 = new BoardState();

            boardState.Add(ChessPosition.A1);
            boardState2.Add(ChessPosition.A1);

            boardState.Add(ChessPosition.D7);
            boardState2.Add(ChessPosition.D7);

            Assert.IsTrue(boardState.Equals(boardState2));
        }

        [TestMethod]
        public void Should_ReturnFalse_For_UnequalBoardStates()
        {
            BoardState boardState = new BoardState();
            BoardState boardState2 = new BoardState();

            boardState.Add(ChessPosition.A1);
            boardState2.Add(ChessPosition.A1);

            boardState.Add(ChessPosition.D7);
            boardState2.Add(ChessPosition.D7);

            boardState.Add(ChessPosition.F7);

            Assert.IsFalse(boardState.Equals(boardState2));
        }

        [TestMethod]
        public void Should_ClearAllPieces_When_ClearMethodIsCalled()
        {
            BoardState boardState = new BoardState();
            boardState.Add(ChessPosition.WhiteStart);
            boardState.Clear();

            bool containsAllWhitePieces = boardState.Contains(ChessPosition.A1) &&
                                          boardState.Contains(ChessPosition.B1) &&
                                          boardState.Contains(ChessPosition.C1) &&
                                          boardState.Contains(ChessPosition.D1) &&
                                          boardState.Contains(ChessPosition.E1) &&
                                          boardState.Contains(ChessPosition.F1) &&
                                          boardState.Contains(ChessPosition.G1) &&
                                          boardState.Contains(ChessPosition.H1) &&
                                          boardState.Contains(ChessPosition.A2) &&
                                          boardState.Contains(ChessPosition.B2) &&
                                          boardState.Contains(ChessPosition.C2) &&
                                          boardState.Contains(ChessPosition.D2) &&
                                          boardState.Contains(ChessPosition.E2) &&
                                          boardState.Contains(ChessPosition.F2) &&
                                          boardState.Contains(ChessPosition.G2) &&
                                          boardState.Contains(ChessPosition.H2) &&
                                          !boardState.Contains(ChessPosition.WhiteStart);

            Assert.IsFalse(containsAllWhitePieces);
        }

        [TestMethod]
        public void Should_ContainAllWhitePieces_When_AddingWhiteStartingPositions()
        {
            BoardState boardState = new BoardState();
            boardState.Add(ChessPosition.WhiteStart);

            bool containsAllWhitePieces = boardState.Contains(ChessPosition.A1) &&
                                          boardState.Contains(ChessPosition.B1) &&
                                          boardState.Contains(ChessPosition.C1) &&
                                          boardState.Contains(ChessPosition.D1) &&
                                          boardState.Contains(ChessPosition.E1) &&
                                          boardState.Contains(ChessPosition.F1) &&
                                          boardState.Contains(ChessPosition.G1) &&
                                          boardState.Contains(ChessPosition.H1) &&
                                          boardState.Contains(ChessPosition.A2) &&
                                          boardState.Contains(ChessPosition.B2) &&
                                          boardState.Contains(ChessPosition.C2) &&
                                          boardState.Contains(ChessPosition.D2) &&
                                          boardState.Contains(ChessPosition.E2) &&
                                          boardState.Contains(ChessPosition.F2) &&
                                          boardState.Contains(ChessPosition.G2) &&
                                          boardState.Contains(ChessPosition.H2) &&
                                          !boardState.Contains(ChessPosition.WhiteStart);


            Assert.IsTrue(containsAllWhitePieces);
        }

        [TestMethod]
        public void Should_ContainBoardStates_When_AddingBoardStates()
        {
            BoardState boardState = new BoardState();
            BoardState whitePieces = new BoardState();
            BoardState blackPieces = new BoardState();

            whitePieces.Add(ChessPosition.WhiteStart);
            blackPieces.Add(ChessPosition.BlackStart);

            boardState.Add(whitePieces);
            boardState.Add(blackPieces);

            bool containsAllWhitePieces = boardState.Contains(ChessPosition.A1) &&
                                          boardState.Contains(ChessPosition.B1) &&
                                          boardState.Contains(ChessPosition.C1) &&
                                          boardState.Contains(ChessPosition.D1) &&
                                          boardState.Contains(ChessPosition.E1) &&
                                          boardState.Contains(ChessPosition.F1) &&
                                          boardState.Contains(ChessPosition.G1) &&
                                          boardState.Contains(ChessPosition.H1) &&
                                          boardState.Contains(ChessPosition.A2) &&
                                          boardState.Contains(ChessPosition.B2) &&
                                          boardState.Contains(ChessPosition.C2) &&
                                          boardState.Contains(ChessPosition.D2) &&
                                          boardState.Contains(ChessPosition.E2) &&
                                          boardState.Contains(ChessPosition.F2) &&
                                          boardState.Contains(ChessPosition.G2) &&
                                          boardState.Contains(ChessPosition.H2) &&
                                          !boardState.Contains(ChessPosition.WhiteStart);

            bool containsAllBlackPieces = boardState.Contains(ChessPosition.A8) &&
                                          boardState.Contains(ChessPosition.B8) &&
                                          boardState.Contains(ChessPosition.C8) &&
                                          boardState.Contains(ChessPosition.D8) &&
                                          boardState.Contains(ChessPosition.E8) &&
                                          boardState.Contains(ChessPosition.F8) &&
                                          boardState.Contains(ChessPosition.G8) &&
                                          boardState.Contains(ChessPosition.H8) &&
                                          boardState.Contains(ChessPosition.A7) &&
                                          boardState.Contains(ChessPosition.B7) &&
                                          boardState.Contains(ChessPosition.C7) &&
                                          boardState.Contains(ChessPosition.D7) &&
                                          boardState.Contains(ChessPosition.E7) &&
                                          boardState.Contains(ChessPosition.F7) &&
                                          boardState.Contains(ChessPosition.G7) &&
                                          boardState.Contains(ChessPosition.H7) &&
                                          !boardState.Contains(ChessPosition.BlackStart);

            Assert.IsTrue(containsAllBlackPieces && containsAllWhitePieces);
        }

        [TestMethod]
        public void Should_NotContainBoardStates_When_RemovingBoardStates()
        {
            BoardState boardState = new BoardState();
            BoardState stateToRemove = new BoardState();

            stateToRemove.Add(ChessPosition.WhiteStart);
            boardState.Add(ChessPosition.WhiteStart);

            boardState.Remove(stateToRemove);

            bool containsAllWhitePieces = boardState.Contains(ChessPosition.A1) &&
                                          boardState.Contains(ChessPosition.B1) &&
                                          boardState.Contains(ChessPosition.C1) &&
                                          boardState.Contains(ChessPosition.D1) &&
                                          boardState.Contains(ChessPosition.E1) &&
                                          boardState.Contains(ChessPosition.F1) &&
                                          boardState.Contains(ChessPosition.G1) &&
                                          boardState.Contains(ChessPosition.H1) &&
                                          boardState.Contains(ChessPosition.A2) &&
                                          boardState.Contains(ChessPosition.B2) &&
                                          boardState.Contains(ChessPosition.C2) &&
                                          boardState.Contains(ChessPosition.D2) &&
                                          boardState.Contains(ChessPosition.E2) &&
                                          boardState.Contains(ChessPosition.F2) &&
                                          boardState.Contains(ChessPosition.G2) &&
                                          boardState.Contains(ChessPosition.H2) &&
                                          !boardState.Contains(ChessPosition.WhiteStart);

            Assert.IsFalse(containsAllWhitePieces);
        }

        [TestMethod]
        public void Should_LoopThroughPositions_For_OccupiedSquaresUsingForeach()
        {
            var boardState = new BoardState
            {
                ChessPosition.A1,
                ChessPosition.A2,
                ChessPosition.A3
            };

            foreach (var position in boardState)
            {
                Assert.IsTrue(position == ChessPosition.A1 || position == ChessPosition.A2 || position == ChessPosition.A3);
            }
        }

        [TestMethod]
        public void Should_ContainOnlyOneCopy_Of_DuplicatePositionAdded()
        {
            var state = new BoardState
            {
                ChessPosition.A1,
                ChessPosition.A1
            };

            int i = 0;

            foreach (ChessPosition s in state)
            {
                i++;
            }

            Assert.AreEqual(1, i);
        }
    }
}
